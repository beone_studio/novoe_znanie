<!DOCTYPE html>
<html>
  <head>
    <title>Новое знание</title>
    <!-- <link rel="stylesheet" href="/assets/index/css/main.css"> -->
    <!-- <link rel="stylesheet" href="/assets/index/css/catalog.css"> -->
		<!-- <link rel="stylesheet" href="/assets/index/css/media.css"> -->
		<!-- <link rel="stylesheet" href="/assets/index/css/mobile_media.css"> -->
    <link rel="stylesheet" href="/assets/index/css/font-awesome.min.css">
		<!-- <link rel="shortcut icon" href="/assets/index/img/favicon.ico"> -->
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="/assets/index/js/jquery-3.2.1.min.js"></script>
	<script src="/assets/index/js/siema.min.js"></script>
	<link rel="stylesheet" href="{{ mix('/assets/index/css/app.css') }}">
  </head>
<body>
	<!-- <div id="main"></div> -->
	<div id="app">
		<search></search>
		<item-slider :url="'allBooks'"></item-slider>
		<item-slider :url="'promotions'"></item-slider>
		<item-slider :url="'new_books'"></item-slider>
	</div>
	<script src="{{ mix('/assets/index/js/app.js') }}"></script>
</body>
</html>