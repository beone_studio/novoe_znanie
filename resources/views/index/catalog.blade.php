@extends('index.layout')

@section('head')
	<title>Новое знание</title>
@endsection

@section('content')
<div class="container mt-30">
	<h2 class="slider-title">
		{{$part->name}}
	</h2>
	<catalog part="{{ $slug }}" filters="{{json_encode($characteristics)}}" url="books/part/{{ $slug }}"/>
</div>

<item-slider name="Новинки" url="books/new/{{ $part->id }}"></item-slider>
@endsection