@extends('index.layout')

@section('head')
	<title>Новое знание</title>
@endsection

@section('content')
<div class="container">
    <div class="categories">
        <div class="category" redirect="/catalog/detskaya">
            <img src="/assets/index/img/new/1.png" alt="">
            <img  class="small" src="/assets/index/img/new/logo_child.png"><h2> Детская <br/> литература</h2>
            <div>
                <p>
                    Умные, яркие, добрые и смешные книги для детей от 1 до 10 лет. Таких
                    вы ещё не видели!
                </p>
                <!--<a class="btn" href="/catalog/detskaya">ПЕРЕЙТИ</a>-->
            </div>
        </div>
        <div class="category" redirect="/catalog/nachalnaya-shkola">
            <img src="/assets/index/img/new/2.png" alt="">
            <h2>Начальная<br/> школа</h2><img class="small" src="/assets/index/img/new/logo_child.png">
            <div>
                <p>
                    Учебные и учебные наглядные пособия по всем
                    предметам для 1-4 класса. Соответствуют календарно-тематическому
                    планированию и программным требованиями. 
                </p>
                <!--<a class="btn" href="/catalog/nachalnaya-shkola">ПЕРЕЙТИ</a>-->
            </div>
        </div>
        <div class="category" redirect="/catalog/srednjaja-shkola">
            <img src="/assets/index/img/new/6.png" alt="">
            <h2>Средняя<br/> школа</h2><img class="small" src="/assets/index/img/new/logo_child.png">
            <div>
                <p>
                    Учебные пособия по всем предметам для 5-11 классов. Соответствуют
                    календарно-тематическому планированию и программным требованиям.
                </p>
                <!--<a class="btn" href="/catalog/nachalnaya-shkola">ПЕРЕЙТИ</a>-->
            </div>
        </div>
        <div class="category" redirect="/catalog/abiturientam">
            <img src="/assets/index/img/new/3.png" alt="">
            <h2>Абитуриентам</h2>
            <div>
                <p>
                    Тестовые задания и справочники для подготовки к поступлению в высшие учебные заведения. 
                </p>
                <!--<a class="btn" href="/catalog/abiturientam">ПЕРЕЙТИ</a>-->
            </div>
        </div>
        <div class="category" redirect="/catalog/vysshaya-shkola">
            <img src="/assets/index/img/new/4.png" alt="">
            <h2>Высшая <br/> школа</h2>
            <div>
                <p>
                    Учись, студент! Литература для студентов и преподавателей. 
                </p>
                <!--<a class="btn" href="/catalog/vysshaya-shkola">ПЕРЕЙТИ</a>-->
            </div>
        </div>
        <div class="category" redirect="/catalog/medicina">
            <img src="/assets/index/img/new/5.png" alt="">
            <h2>Медицинская <br/> литература</h2>
            <div>
                <p>
                    Книги медицинской тематики, предназначенные для врачей и студентов.
                </p>
                <!--<a class="btn" href="/catalog/medicina">ПЕРЕЙТИ</a>-->
            </div>
        </div>
        <div class="bigcategory" redirect="/catalog/shelkovyj-put">
            <img src="/assets/index/img/new/7.png" alt="">
            <h2>Шёлковый путь</h2>
            <div>
                <p>
                    Переводная литература с китайского языка.
                </p>
                <!--<a class="btn" href="/catalog/medicina">ПЕРЕЙТИ</a>-->
            </div>
        </div>
    </div>
</div>

<item-slider name="Новинки" :url="'books/new/0'"></item-slider>

<item-slider name="Акции" :url="'books/promotions'"></item-slider>

@if (count($news) > 0)
<div class="container">
    <h2 class="slider-title mt-30 mb-30">Новости</h2>
    <div class="news-list">
        @foreach($news as $article)
        <div class="article">
            <a href="/news/{{$article->slug}}">
                <div class="image-container">
                    <img src="{{$article->image_url}}" alt="">
                </div>
            </a>
            <div class="article-info">
                <a href="/news/{{$article->slug}}">
                    <h3 class="news-title">
                        {{$article->name}}
                    </h3>
                </a>
            <p class="date">
                {{$article->created_at->format('d.m.Y')}}
            </p>    
            <a href="/news/{{$article->slug}}" class="article-link">Узнать больше</a>
            </div>
        </div>
        @endforeach
    </div>
</div>
@endif

@endsection