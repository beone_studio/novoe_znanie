@extends('index.layout')

@section('head')
	<title>Новое знание</title>
@endsection

@section('content')
<div class="container flex-between-top">
	<div class="inner-block">
		<div class="image-container">
			<img src="{{$article->image_url}}" alt="">
		</div>
		<h1 class="image-header">
			{{$article->name}}
			<p class="image-date">
				{{$article->created_at->format('d.m.Y')}}
			</p>
		</h1>
		<div class="information">
			{!! $article->info !!}
		</div>
		@if ($article->video1 ||$article->video2)
		<h1 class="slider-title mt-10">Видео</h2>
		<div class="videos">
			@if ($article->video1)
			<div class="video1">
				<iframe width="100%" style="margin-bottom: 20px;" src="https://www.youtube.com/embed/{{$article->youtube_link}}">
				</iframe>	
				<a target="_blank" class="video_link" href="https://www.youtube.com/embed/{{$article->youtube_link}}">Перейти на YouTube</a>		
			</div>
			@endif
			@if ($article->video2)
			<div class="video2">
				<iframe width="100%" style="margin-bottom: 20px;" src="https://www.youtube.com/embed/{{$article->youtube_link}}">
				</iframe>		
				<a target="_blank" class="video_link" href="https://www.youtube.com/embed/{{$article->youtube_link}}">Перейти на YouTube</a>	
			</div>
			@endif		
		</div>
		@endif
	</div>
	<div class="other-news">
		@foreach($last_four_news as $latest)
		<a href="/news/{{$latest->slug}}" style="color: rgb(30, 30, 30);">
			<div class="last-news">
				<div class="image-container">
					<img src="{{$latest->image_url}}" alt="">
				</div>
				<div class="info">
					<h2 class="news-title">{{$latest->name}}</h2>
				</div>
			</div>
		</a>
		@endforeach
		<a href="/news?page=1" class="all-news">Все новости</a>
	</div>
</div>
@endsection