@extends('index.layout')

@section('head')
	<title>Новое знание</title>
@endsection

@section('content')
	<div class="container flex-between mt-50 align-top">
		<div class="inner-info">
			<h2 class="slider-title">{{ $book-> name }}</h2>
			<h3 class="inner-author mt-10">
				{{ $book-> author }}
			</h3>
			<h4 class="inner-props mt-30">
				Характеристики
			</h4>
			<div class="characters mt-30">
				@if ($book-> book_id)
					<div class="char">
						<p>ID товара</p>
						<p>627447</p>
					</div>
				@endif
				@if ($book-> isbn)
				<div class="char">
					<p>ISBN</p>
					<p>{{ $book-> isbn }}</p>
				</div>
				@endif
				@if ($book-> year)
				<div class="char">
					<p>Год издания</p>
					<p>{{ $book-> year }}</p>
				</div>
				@endif
				@if ($book-> page_number)
				<div class="char">
					<p>Кол-во страниц</p>
					<p>{{ $book-> page_number }}</p>
				</div>
				@endif
				@if ($book-> cover_type)
				<div class="char">
					<p>Тип обложки</p>
					<p>{{ $book-> cover_type }}</p>
				</div>
				@endif
				@if ($book-> decor)
				<div class="char">
					<p>Оформление</p>
					<p>{{ $book-> decor }}</p>
				</div>
				@endif
				@if ($book-> illustration)
				<div class="char">
					<p>Иллюстрации</p>
					<p>{{ $book-> illustration }}</p>
				</div>
				@endif
				@if ($book-> weight)
				<div class="char">
					<p>Масса</p>
					<p>{{ $book-> weight }} г.</p>
				</div>
				@endif
				@if ($book-> width && $book-> length && $book-> thickness)
				<div class="char">
					<p>Размеры</p>
					<p>{{ $book-> width }}*{{ $book-> length }}*{{ $book-> thickness }} мм</p>
				</div>
				@endif
			</div>
			<div class="mt-50 flex-align-end">
				<a id="add-to-cart"
				   class="buy"
				   data-book-id="{{ $book->id }}">
					В корзину
				</a>
				<span class="price">{{ $book-> price }} <span>BYN</span></span>
			</div>
		</div>
		<div  class="inner-album">
			<div class="albumcontainer mt-10">
				<img src="{{ $book-> image_url }}" alt="">
				<img class="shadow" src="/assets/index/img/shadow.png" alt="">
			</div>
			@if ($book->video)
			<div class="bookVideo">
				<a data-popup-open="popup-video" class="buy">Видео о книге</a>
			</div>
			@endif

			@if ($book->preview || ($book->pdf_url_service && $book->pdf_url_service != 'null'))
			<div class="bookPreview">
				<a data-popup-open="popup-preview">
					<span class="fa fa-eye"></span>
					<span class="text">Пролистать <br> книгу</span>
				</a>
			</div>
			@endif
		</div>
	</div>

	<div class="container">
		<div class="tab-container">
			<div class="table-head">
				@if ($book-> about_book)
				<a class="active" id="description">Описание</a>
				@endif
				@if ($book-> additional_info)
				<a id="characteristics">Доп. материалы</a>
				@endif
				@if (count($audios) > 0)
				<a id="audio">Аудио материалы</a>
				@endif
			</div>	
			<div class="table-body" id="inDescription">
				<div class="table-element">
					<div class="inner-descr">
						{!! $book-> about_book !!}
					</div>
				</div>
			</div>	
			<div class="table-body" id="inCharacteristics" style="display: none">
				<div class="table-element"> 
					<div class="inner-descr">
					{!! $book-> additional_info !!}
					</div>
				</div>	
			</div>
			<div class="table-body" id="inAudio" style="display: none">
				<div class="table-element"> 
					<div class="inner-descr">
						@foreach($audios as $audio)
							<label>{{$audio->alias}}</label>
							<audio controls="controls">
								<source src="{{$audio->link}}">
							</audio>
						@endforeach
					</div>
				</div>	
			</div>
		</div>
	</div>

	<item-slider name="Также в этой серии" :url="'books/{{ $book->slug }}/same_books'"></item-slider>

	<div class="container">
		<h2 class="slider-title mt-50">Отзывы</h2>
		<div class=" mt-30 create_review">
			<form role="form" method="post" action="/review">
				<h4 class="absolute-title-tr">Оставьте отзыв</h4>
				<input type="hidden" name="product_id" value="{{ $book->id }}">
				<input type="text" name="user_name" placeholder="Ваше имя">
				<input type="text" name="contacts" placeholder="Ваше телефон или email">
				<textarea style="line-height: 1.2em; font-size: 14px"  name="review" id="" cols="30" rows="10" placeholder="Ваш отзыв">
				</textarea>
				<button type="submit">ОСТАВИТЬ ОТЗЫВ</button>
			</form>
		</div>
		<div class="reviews mt-30">
			<h4 class="absolute-title-tl">Отзывы других пользователей</h4>
			<div class="reviews_container mt-10">
				@foreach($comments as $comment)
				<div class="review">
					<i class="fa fa-user-o"></i>
					<h3>{{ $comment->user_name }}</h3>
					<p>
						{{ $comment->review }}
					</p>
				</div>
				@endforeach
			</div>
		</div>
	</div>

	<div class="popup" data-popup="popup-video">
		<div class="popup-inner video">
			<h2 class="popUpTitle">Видео о книге</h2>
			@if ($book->video)
				<iframe width="100%" style="margin-bottom: 20px;" src="https://www.youtube.com/embed/{{ $book->video }}">
				</iframe>
			@endif
			<a class="popup-close" data-popup-close="popup-video" href="#">x</a>
		</div>
	</div>

	@if($book->preview || $book->pdf_url_service)
	<div class="popup" data-popup="popup-preview" >
		<div class="popup-inner full">
			@if($book->preview)
				<embed class="mt-30" src="{{$book->preview_url}}#page=1&zoom=60" width="100%" height="90%">
			@endif
			@if ($book->pdf_url_service && $book->pdf_url_service != 'null')
			<div class="mt-30 pdf_service">
			<div style="text-align:center;">
				<div style="margin:8px 0px 4px;">
					<a href="https://www.calameo.com/books/{{$book->pdf_url_service}}" target="_blank">НЕО ХУДОЖНИК</a></div>
					<iframe src="//v.calameo.com/?bkcode={{$book->pdf_url_service}}" width="900" height="500" frameborder="0" scrolling="no" allowtransparency allowfullscreen style="margin:0 auto;"></iframe>
					<div style="margin:4px 0px 8px;"><a href="http://www.calameo.com/">Publish at Calameo</a></div>
				</div>
			</div>
			@endif
			<a class="popup-close" data-popup-close="popup-preview" href="#">x</a>
		</div>
	</div>
	@endif
@endsection

@section('scriptContent')
	<script lang="js">
		var button = document.getElementById('add-to-cart');
		var book = {!! json_encode($book) !!};
		var bookId = book.id;
		var cartStorage = localStorage.getItem('cart') || null;
		var cart = cartStorage ? cartStorage.split(',') : [];
		if (cart.indexOf(bookId.toString()) === -1) {
			button.addEventListener('click', function(e) {
				cart.push(bookId);
				localStorage.setItem('cart', cart.join(','));
				button.classList.add('in-cart');
				button.innerHTML = "В Корзине";
			});
		} else {
			button.classList.add('in-cart');
			button.innerHTML = "В Корзине";
		}
	</script>
@endsection