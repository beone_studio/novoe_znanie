@extends('index.layout')


@section('head')
	<title>Новое знание</title>
@endsection

@section('content')
<div class="container">
	<div class="about-info mt-10">
		<h2 class="about-title">
			О нас
		</h2>
		<div class="descr mt-30">
		Издательская группа «Новое знание» и «Открытая книга» более 20 лет занимается изданием
		детской, школьной и университетской литературой.
		<br><br>
		За время нашей работы в свет вышло около 1000 книг. Наши авторы — преподаватели ведущих
		вузов страны, высококвалифицированные учителя-практики, авторитетные специалисты в
		различных областях науки.
		<br><br>
		В 2017 году мы открыли новое направление – детская литература. Мы хотим предложить самые
		яркие, интересные и необычные детские книги, полюбившиеся малышам и родителям всего
		мира.
		<br><br>
		Теперь читатели могут найти на полках магазинов книгу &quot;Все видели кота&quot;, признанной
		американской ассоциацией лучшей детской книгой 2017 года, французские бестселлеры «Кто
		такой Кто» и «Кто такой Какой», немецкие вельмельбухи про неудачливого «Мага Захарию» и
		многие другие!
		<br><br>
		Подписывайтесь на нас в социальных сетях и следите за новостями и новинками нашего
		издательства!
		</div>
	</div>
	<div class="contact-info mt-30">
		<h2 class="slider-title">Контакты</h2>
		<div class="phones">
			<div class="flex-between-top">
				<h4 class="phones-header">ТЕЛЕФОНЫ</h4>
				<div class="numbers">
					<p>+375 (29) 3-640-640</p>
					<p>+375 (17) 360-20-02</p>
				</div>
			</div>
		</div>
		<div class="email">
			<div class="flex-between-top">
				<h4 class="email-header">ПОЧТА</h4>
				<div class="email-addresses">
					<p>sale@wnk.biz</p>
					<p>editor@wnk.biz</p>
				</div>
			</div>
		</div>
		<div class="address">
			<div class="flex-between-top">
				<h4 class="address-header">АДРЕС</h4>
				<div class="addresses">
					<p>пр. Пушкина, д. 15а<br> <br> 220015, г. Минск</p>
				</div>	
			</div>		
		</div>
		<div class="soc-networks">
			<div class="flex-between-top">
				<h4 class="soc-networks-header">ОТКРЫТАЯ КНИГА</h4>
				<div class="networks">
					<a target="_blank" href="https://www.instagram.com/otkrytaya_kniga_izd/"><span class="fa fa-instagram"></span></a>
					<a target="_blank" href="https://vk.com/otkrytaya_kniga"><span class="fa fa-vk"></span></a>
				</div>	
			</div>		
		</div>
		<div class="soc-networks">
			<div class="flex-between-top">
				<h4 class="soc-networks-header">НОВОЕ ЗНАНИЕ</h4>
				<div class="networks">
					<a target="_blank" class="trird" href="https://www.facebook.com/novoeznaniedetyam/"><span class="fa fa-facebook"></span></a>
					<a target="_blank" href="https://vk.com/club120761323"><span class="fa fa-vk"></span></a>
					<a target="_blank" href="https://www.youtube.com/channel/UC7w6jn84MXt8zXRzXmg2qhg"><span class="fa fa-youtube"></span></a>
					<br/>
					<a target="_blank" href="https://t.me/newknoledge"><span class="fa fa-telegram"></span></a>
					<a target="_blank" class="fifth" href="https://ok.ru/group/54044835446909"><span class="fa fa-odnoklassniki"></span></a>
				</div>	
			</div>		
		</div>
	</div>
</div>
<div class="flex-between-top about-us">
	<div class="map mt-50" id="googleMap">		
	</div>
</div>
@endsection


@section('scriptContent')
	<script>
		function myMap() {
			var map = new google.maps.Map(document.getElementById('googleMap'), {
				center: {lat: 53.8980833, lng: 27.49529240000004},
				zoom: 17,
				styles: [
							{
									"featureType": "administrative",
									"elementType": "all",
									"stylers": [
											{
													"saturation": "-100"
											}
									]
							},
							{
									"featureType": "administrative.province",
									"elementType": "all",
									"stylers": [
											{
													"visibility": "off"
											}
									]
							},
							{
									"featureType": "landscape",
									"elementType": "all",
									"stylers": [
											{
													"saturation": -100
											},
											{
													"lightness": 65
											},
											{
													"visibility": "on"
											}
									]
							},
							{
									"featureType": "poi",
									"elementType": "all",
									"stylers": [
											{
													"saturation": -100
											},
											{
													"lightness": "50"
											},
											{
													"visibility": "simplified"
											}
									]
							},
							{
									"featureType": "road",
									"elementType": "all",
									"stylers": [
											{
													"saturation": "-100"
											}
									]
							},
							{
									"featureType": "road.highway",
									"elementType": "all",
									"stylers": [
											{
													"visibility": "simplified"
											}
									]
							},
							{
									"featureType": "road.arterial",
									"elementType": "all",
									"stylers": [
											{
													"lightness": "30"
											}
									]
							},
							{
									"featureType": "road.local",
									"elementType": "all",
									"stylers": [
											{
													"lightness": "40"
											}
									]
							},
							{
									"featureType": "transit",
									"elementType": "all",
									"stylers": [
											{
													"saturation": -100
											},
											{
													"visibility": "simplified"
											}
									]
							},
							{
									"featureType": "water",
									"elementType": "geometry",
									"stylers": [
											{
													"hue": "#ffff00"
											},
											{
													"lightness": -25
											},
											{
													"saturation": -97
											}
									]
							},
							{
									"featureType": "water",
									"elementType": "labels",
									"stylers": [
											{
													"lightness": -25
											},
											{
													"saturation": -100
											}
									]
							}
					]
			});
			var marker = new google.maps.Marker({
				position: {lat: 53.8980833, lng: 27.49529240000004},
				map: map,
				title: 'Издательство "Новое знание"'
			});
		}
	</script>
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCJcKXsZrXwdbXx2CM08n3AHl9frIHxU1w&callback=myMap"></script>
@endsection