@extends('index.layout')

@section('head')
    <title>Новое знание | Оформление заказа</title>
@endsection

@section('content')
    <div class="container">
        <h2 class="slider-title mt-30">Оформление заказа</h2>

        <cart order></cart>
    </div>
@endsection