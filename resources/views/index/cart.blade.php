@extends('index.layout')

@section('head')
    <title>Новое знание | Корзина</title>
@endsection

@section('content')
    <div class="container">
        <h2 class="slider-title mt-30">Корзина</h2>

        <cart></cart>
    </div>
@endsection