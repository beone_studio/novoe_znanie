<!DOCTYPE html>
<html>
  <head>
	@yield('head')
	<link rel="icon" href="/assets/index/img/logo-min.png">
    <link rel="stylesheet" href="/assets/index/css/main.css">
    <link rel="stylesheet" href="/assets/index/css/font-awesome.min.css">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="{{ mix('/assets/index/css/app.css') }}">
	<script src="/assets/index/js/jquery-3.2.1.min.js"></script>
	<script src="/assets/index/js/siema.min.js"></script>

	<!-- Global site tag (gtag.js) - Google Analytics --> 
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-120386556-2"></script> 
	<script> 
		window.dataLayer = window.dataLayer || []; 
		function gtag(){dataLayer.push(arguments);} 
		gtag('js', new Date()); 

		gtag('config', 'UA-120386556-2'); 
	</script>
	<!-- /Global site tag (gtag.js) - Google Analytics -->

	<!-- Yandex.Metrika informer--> 
		<!-- <a href="https://metrika.yandex.ru/stat/?id=49145440&amp;from=informer" target="_blank" rel="nofollow">
			<img src="https://metrika-informer.com/informer/49145440/3_0_FFFFFFFF_FFFFFFFF_0_visits&quot" 
				style="width:88px; height:31px; border:0;" 
				alt="Яндекс.Метрика" 
				title="Яндекс.Метрика: данные за сегодня (просмотры, визиты и уникальные посетители)" 
				class="ym-advanced-informer" 
				data-cid="49145440" 
				data-lang="ru" />
		</a> -->
	<!-- /Yandex.Metrika informer --> 

	<!-- Yandex.Metrika counter --> 
		<script type="text/javascript" > 
			(function (d, w, c) { 
			(w[c] = w[c] || []).push(function() { 
			try { 
			w.yaCounter49145440 = new Ya.Metrika2({ 
			id:49145440, 
			clickmap:true, 
			trackLinks:true, 
			accurateTrackBounce:true, 
			webvisor:true, 
			trackHash:true 
			}); 
			} catch(e) { } 
			}); 

			var n = d.getElementsByTagName("script")[0], 
			s = d.createElement("script"), 
			f = function () { n.parentNode.insertBefore(s, n); }; 
			s.type = "text/javascript"; 
			s.async = true; 
			s.src = "https://cdn.jsdelivr.net/npm/yandex-metrica-watch/tag.js";; 

			if (w.opera == "[object Opera]") { 
			d.addEventListener("DOMContentLoaded", f, false); 
			} else { f(); } 
			})(document, window, "yandex_metrika_callbacks2"); 
		</script> 
		<noscript><div><img src="https://mc.yandex.ru/watch/49145440"; style="position:absolute; left:-9999px;" alt="" /></div></noscript> 
	<!-- /Yandex.Metrika counter -->
  </head>
<body>

<div id="app">
	<header>
		  @if(Session::has('flash_message'))
		  <div id="flash_message">
		    <span class="fa fa-check" style="margin-right: 10px;font-size: 24px;"></span>{{ Session::get('flash_message') }}
		  </div>
		  @endif
		<div class="container">
			<div class="telefons">
				<a href="">+375 (29) 3-640-640</a>
				<a href="">+375 (17) 360-20-02</a>
			</div>
		</div>

		<div class="container flex-between">
			<div class="logo">
				<a href="/">
					<img id="logo" src="/assets/index/img/new/logo.png" alt="">
					<img id="logo_white" src="/assets/index/img/new/logo_white.png" alt="">
				</a>
			</div>
			<div class="searchBlock">
				<search></search>
			</div>
			<div class="menu">
				<ul>
					<li id="dropdown">
						<a id="catalog">Каталог</a>
						<ul class="dropdown-menu">
							<li><a href="/catalog/detskaya">Детская литература</a></li>
							<li><a href="/catalog/nachalnaya-shkola">Начальная школа</a></li>
							<li><a href="/catalog/srednjaja-shkola">Средняя школа</a></li>
							<li><a href="/catalog/abiturientam">Абитуриентам</a></li>
							<li><a href="/catalog/vysshaya-shkola">Высшая школа</a></li>
							<li><a href="/catalog/medicina">Мед. литература</a></li>
							<li><a href="/catalog/shelkovyj-put">Шёлковый путь</a></li>
						</ul>
					</li>
					<li><a href="/news">Новости</a></li>
					<li><a href="/contacts">Контакты</a></li>
					<li class="cart-link">
						<a href="/cart">
							Корзина
							<i class="fa fa-shopping-cart" aria-hidden="true"></i>
						</a>
					</li>
				</ul>
			</div>		
		</div>

	</header>

	@yield('content')


	<footer>

		<div class="container flex-between">
			<div class="firstBlock">
				<div class="partners">
					<p>Наши проекты</p>
					<a target="_blank" href="http://1-4.by/">
						<img src="/assets/index/img/1_4by.ico" alt="Партнер издательства 'Новое Знание' - 'www.1-4.by'">
						<span>Портал для учителей </br> «1-4.by»</span>
					</a>
					<a target="_blank" href="http://vedy.by/">
						<img src="/assets/index/img/or.ico" alt="Партнер издательства 'Новое Знание' - 'Репетитор онлайн'">
						<span>Репетитор онлайн</span>
					</a>
				</div>
			</div>
			<div class="secondBlock">
				<div class="categories">
					<a href="/catalog/detskaya">Детская литература</a>
					<a href="/catalog/nachalnaya-shkola">Начальная школа</a>
					<a href="/catalog/srednjaja-shkola">Средняя школа</a>
					<a href="/catalog/abiturientam">Абитуриентам</a>
					<a href="/catalog/vysshaya-shkola">Высшая школа</a>
					<a href="/catalog/medicina">Мед. литература</a>
					<a href="/catalog/shelkovyj-put">Шёлковый путь</a>
				</div>
				<div class="undercategories mt-30">
					<div class="email">
						<form role="form" method="post" action="/notifications">
							<label for="">Введите e-mail, чтобы подписаться на рассылку</label>
							<input type="text" name="mail" placeholder="e-mail">
							<button type="submit"><span class="fa fa-envelope"></span></button>
						</form>
					</div>
					<div class="footer-telefons">
						<p>Наши контакты</p>
						<div class="block">
							<a href="">+375 (29) 3-640-640</a>
							<a href="">+375 (17) 360-20-02</a>
						</div>
						<div class="block">
							<a href="mailto:sale@wnk.biz">sale@wnk.biz</a>
							<a href="mailto:editor@wnk.biz">editor@wnk.biz</a>	
						</div>
					</div>
				</div>
			</div>
			<div class="thirdBlock">
				<div class="menu">
					<li><a href="/">Главная</a></li>
					<li><a href="/news">Новости</a></li>
					<li><a href="/contacts">Контакты</a></li>
				</div>
				<div class="soc-networks mt-30">
					<div class="block">
						<p>Открытая книга</p>
						<!-- <p>ул. Нововиленская, д.5 <br> г.Минск 220056</p> -->
						<a target="_blank" href="https://www.instagram.com/otkrytaya_kniga_izd/"><span class="fa fa-instagram"></span></a>
						<a target="_blank" href="https://vk.com/otkrytaya_kniga"><span class="fa fa-vk"></span></a>
					</div>
					<div class="block">
						<p>Новое знание</p>
						<a target="_blank"  href="https://www.facebook.com/novoeznaniedetyam/"><span class="fa fa-facebook"></span></a>
						<a target="_blank" href="https://vk.com/club120761323"><span class="fa fa-vk"></span></a>
						<a target="_blank" href="https://www.youtube.com/channel/UC7w6jn84MXt8zXRzXmg2qhg"><span class="fa fa-youtube"></span></a>
						<a target="_blank" href="https://t.me/newknoledge"><span class="fa fa-telegram"></span></a>
						<a target="_blank" href="https://ok.ru/group/54044835446909"><span class="fa fa-odnoklassniki"></span></a>
					</div>
				</div>
			</div>
		</div>
	</footer>
	<div class="copyright">
		Издательство "Новое знание" &copy;. 2018.
	</div>
</div>	
<script src="{{ mix('/assets/index/js/app.js') }}"></script>
<script>
	$('#flash_message').delay(4000).slideUp(400);

	$(function() {
		//----- OPEN
		$('[data-popup-open]').on('click', function(e)  {
			var targeted_popup_class = jQuery(this).attr('data-popup-open');
			$('[data-popup="' + targeted_popup_class + '"]').fadeIn(350);
		
			e.preventDefault();
		});
		
		//----- CLOSE
		$('[data-popup-close]').on('click', function(e)  {
			var targeted_popup_class = jQuery(this).attr('data-popup-close');
			$('[data-popup="' + targeted_popup_class + '"]').fadeOut(350);
		
			e.preventDefault();
		});

		$('[redirect]').on('click', function(e)  {
			var link = jQuery(this).attr('redirect');
			window.location = link;
		
			e.preventDefault();
		});

	});	

	window.onload = function() {
		var path = window.location.pathname;
		var customClass = path.slice(path.lastIndexOf('/') + 1);
		if (customClass === 'order') customClass = 'cart';
		
		$('header').addClass(customClass);
		if ($('header').css('background').indexOf('rgba(0, 0, 0, 0)') === -1) {
			$('#logo_white').css('display', 'block');
		} else {
			$('#logo').css('display', 'block');
		}
		
		$('html,body').css('opacity', '1');
	};

	$('#catalog').click(function(event) {
		event.preventDefault();
		if ($(window).width() < 960) {
			$('.dropdown-menu').css('display', 'block');
		};
	});

	document.addEventListener('scroll', function () {
		if ($(window).width() < 960) {
			$('.dropdown-menu').css('display', 'none');
		};
	});

	document.addEventListener('click', function (event) {
		if ($(window).width() < 960) {
			if (event.target.getAttribute('id') !== 'catalog') {
				$('.dropdown-menu').css('display', 'none');
			}
		};
	});

	$('#description').click(function(event) {
		event.preventDefault();
		$('#characteristics').removeClass('active');
		$('#audio').removeClass('active');
		$('#description').addClass('active');
		$('#inDescription').css('display', 'block');
		$('#inCharacteristics').css('display', 'none');
		$('#inAudio').css('display', 'none');

	});
	$('#characteristics').click(function(event) {
		event.preventDefault();
		$('#characteristics').addClass('active');
		$('#description').removeClass('active');
		$('#audio').removeClass('active');
		$('#inDescription').css('display', 'none');
		$('#inAudio').css('display', 'none');
		$('#inCharacteristics').css('display', 'block');
	});

	$('#audio').click(function(event) {
		event.preventDefault();
		$('#audio').addClass('active');
		$('#description').removeClass('active');
		$('#characteristics').removeClass('active');
		$('#inAudio').css('display', 'block');
		$('#inCharacteristics').css('display', 'none');
		$('#inDescription').css('display', 'none');
	});
	</script>
	@yield('scriptContent')
	</body>
</html>