@extends('index.layout')

@section('head')
	<title>Новое знание</title>
@endsection

@section('content')
<div class="container">
  <h2 class="slider-title mt-30">Новости</h2>
  <div class="news-list mt-30">
    @foreach($news as $article)
    <div class="article">
      <a href="/news/{{$article->slug}}">
        <div class="image-container">
          <img src="{{$article->image_url}}" alt="">
        </div>
      </a>
      <div class="article-info">
        <a href="/news/{{$article->slug}}">
          <h3 class="news-title mt-10">
            {{$article->name}}
          </h3>
        </a>
        <p class="date">
          {{$article->created_at->format('d.m.Y')}}
        </p>
        <a href="/news/{{$article->slug}}" class="article-link">Узнать больше</a>
      </div>
    </div>
    @endforeach  
    {{ $news->links() }}
  </div>
</div>
@endsection