@extends('index.layout')

@section('head')
    <title>Новое знание | Прайс лист | Начальная школа</title>
@endsection

@section('content')
    <div class="container mt-30">
        <h2 class="slider-title">
            Прайс лист | {{$part->name}}
        </h2>
        <price-list part="{{ $part->slug }}" filters="{{json_encode($characteristics)}}" url="books/part/{{ $part->slug }}"/>
    </div>
@endsection