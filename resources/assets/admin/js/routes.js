import products from './components/products'
import news from './components/news'
import promotions from './components/promotions'
import new_books from './components/new_books'
import categories from './components/categories'
import characteristics from './components/characteristics'
import recommended from './components/recommended'
import filters from './components/filters'
import banners from './components/banners'

export default [
	...characteristics,
	...filters,
	// ...categories,
	...banners,
    ...products,
    ...promotions,
    // ...recommended,
    ...new_books,
    ...news
]