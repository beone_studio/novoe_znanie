import 'babel-polyfill'
import Vue from 'vue'
import VueRouter from 'vue-router'
import App from './components/app.vue'
import axios from 'axios'
import VueFlashMessage from 'vue-flash-message'
import VueDraggable from 'vue-draggable'
require('./bootstrap');
require('vue-flash-message/dist/vue-flash-message.min.css');

Vue.use(VueFlashMessage, {
    messageOptions: {
      timeout: 1000
    }
  });

axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
let token = document.head.querySelector('meta[name="csrf-token"]');
axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;

Vue.use(VueRouter);
Vue.use(VueDraggable);

Vue.prototype.$http = {
    get(url, data) {
        return axios.get(`/admin/api/${url}`, { params: data }).then(response => response.data)
    },

    post(url, data) {
        return axios.post(`/admin/api/${url}`, data).then(response => response.data)
    },

    delete (url) {
        return axios.delete(`/admin/api/${url}`).then(response => response.data)
    },
}

const app = new Vue({
    render: h => h(App),
}).$mount('#app')
