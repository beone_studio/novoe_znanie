import Table from './table.vue'

export default [
    {
        path: '/promotions',
        component: Table,
        props: {
            title: 'Акции',
            icon: 'bullhorn',
        },
        children: []
    }      
]