import Table from './table.vue'

export default [
    {
        path: '/banners',
        component: Table,
        props: {
            title: 'Баннеры',
            icon: 'file-image-o',
        },
        children: []
    }      
]