import Create from './create.vue'
import Table from './table.vue'
import Edit from './edit.vue'
import Comments from './comments.vue'
import Characteristics from './characteristics.vue'
import Layout from './layout.vue'
import Filters from './parts_categories_filters.vue'
import SameProducts from './same_series.vue'

export default [
    {
        path: '/products',
        component: Layout,
        props: {
            title: 'Книги',
            icon: 'product-hunt',
        },
        children: [ 
            {
                path: '', 
                name: 'Список книг',
                component: Table, 
                props: {
                    title: 'Список книг',
                    showInMenu: true
                }
            },
            {
                path: 'create', 
                name: 'Создание книги',
                component: Create,
                props: {
                    title: 'Создание книги',
                    showInMenu: true
                }                
            },
            {
                path: ':id', 
                name: 'Редактирование книги',
                component: Edit,
                props: {
                    title: 'Редактирование книги',
                    showInMenu: false
                }                
            },
            {
                path: ':id/comments', 
                name: 'Комментарии к книге',
                component: Comments,
                props: {
                    title: 'Комментарии к книге',
                    showInMenu: false
                }                
            },
            {
                path: ':id/characteristics', 
                name: 'Характеристики книги',
                component: Characteristics,
                props: {
                    title: 'Характеристики книги',
                    showInMenu: false
                }                
            },
            {
                path: ':id/filters', 
                name: 'Фильтры книги',
                component: Filters,
                props: {
                    title: 'Фильтры книги',
                    showInMenu: false
                }
            },
            {
                path: ':id/same_products', 
                name: 'Товары этой же серии',
                component: SameProducts,
                props: {
                    title: 'Товары этой же серии',
                    showInMenu: false
                }
            }                         
        ]        
    }      
]