import Table from './table.vue'

export default [
    {
        path: '/new_books',
        component: Table,
        props: {
            title: 'Новинки',
            icon: 'list',
        },
        children: []
    }      
]