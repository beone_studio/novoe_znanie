import Table from './table.vue'

export default [
    {
        path: '/recommended',
        component: Table,
        props: {
            title: 'Рекомендуемое',
            icon: '',
        },
        children: []
    }      
]