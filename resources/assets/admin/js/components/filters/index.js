import Create from './create.vue'
import Table from './table.vue'
import Edit from './edit.vue'
import Layout from './layout.vue'

export default [
    {
        path: '/filters',
        component: Layout,
        props: {
            title: 'Фильтры',
            icon: '',
        },   
        children: [
            {
                path: '', 
                name: 'Список фильтров',
                component: Table, 
                props: {
                    title: 'Список фильтров',
                    showInMenu: true
                },
            },
            {
                path: 'create', 
                name: 'Создание фильтров',
                component: Create,
                props: {
                    title: 'Создание фильтров',
                    showInMenu: true
                }                
            },
            {
                path: ':id', 
                name: 'Редактирование фильтров',
                component: Edit, 
                props: {
                    title: 'Редактирование фильтров',
                    showInMenu: false
                }
            }                   
        ] 
    }      
]