import Create from './create.vue'
import Table from './table.vue'
import Edit from './edit.vue'
import Layout from './layout.vue'

export default [
    {
        path: '/news',
        component: Layout,
        props: {
            title: 'Новости',
            icon: 'newspaper-o',
        },     
        children: [
            {
                path: '', 
                name: 'Список новостей',
                component: Table, 
                props: {
                    title: 'Список новостей',
                    showInMenu: true
                }
            },
            {
                path: 'create', 
                name: 'Создание новости',
                component: Create,
                props: {
                    title: 'Создание новости',
                    showInMenu: true
                }                
            },
            {
                path: ':id', 
                name: 'Редактирование новости',
                component: Edit,
                props: {
                    title: 'Редактирование новости',
                    showInMenu: false
                }                
            },                          
        ]   
    }     
]