import Create from './create.vue'
import Table from './table.vue'
import Edit from './edit.vue'
import Layout from './layout.vue'

export default [
    {
        path: '/categories',
        component: Layout,
        props: {
            title: 'Категории',
            icon: 'vk',
        },   
        children: [
            {
                path: '', 
                name: 'Список категорий',
                component: Table, 
                props: {
                    title: 'Список категорий',
                    showInMenu: true
                },
            },
            {
                path: 'create', 
                name: 'Создание категории',
                component: Create,
                props: {
                    title: 'Создание категории',
                    showInMenu: true
                }                
            },
            {
                path: ':id', 
                name: 'Редактирование категории',
                component: Edit, 
                props: {
                    title: 'Редактирование категории',
                    showInMenu: false
                }
            }                   
        ] 
    }      
]