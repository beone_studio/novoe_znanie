import 'babel-polyfill'
import Vue from 'vue'
import Router from 'vue-router'
import axios from 'axios'
require('./bootstrap');

Vue.use(Router);

Vue.prototype.$http = {
    get(url, data) {
        return axios.get(`/${url}`, { params: data }).then(response => response.data)
    },

    post(url, data) {
        return axios.post(`/${url}`, data).then(response => response.data)
    },

    delete (url) {
        return axios.delete(`/${url}`).then(response => response.data)
    },
};

const router = new Router({
  mode: 'history'
});

var app = new Vue({
   el: '#app',
   router,
   components: {
     'search': require('./components/search_engine/search.vue'),
     'item-slider': require('./components/itemSlider.vue'),
     'catalog': require('./components/catalog.vue'),
     'cart': require('./components/cart.vue'),
     'price-list': require('./components/priceList')
   }
});
