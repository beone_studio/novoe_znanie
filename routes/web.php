<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use App\CharacteristicType;

Route::post('/send-order', 'EmailController@sendOrder');
Route::post('/notifications', 'EmailController@notifications');
Route::post('/review', 'ReviewController@postReview');

Route::group(['prefix' => 'admin', 'namespace' => 'Admin'], function () {
    Route::get('/', 'AuthController@login');
    Route::post('/', 'AuthController@doLogin');
    Route::get('/logout', 'AuthController@logout');

    Route::group(['middleware' => 'auth'], function () {
        Route::group(['prefix' => 'api'], function () {
            Route::get('/allCategories', 'CategoriesController@getAllCategories');
            Route::group(['prefix' => 'categories'], function () {
                Route::get('/', 'CategoriesController@getSubCategories');
                Route::get('/{category}', 'CategoriesController@getSubCategories');
                Route::delete('/{id}', 'CategoriesController@delete');
                Route::post('/', 'CategoriesController@create');
                Route::post('/{category}', 'CategoriesController@update');
            });
            Route::group(['prefix' => 'products'], function () {
                Route::get('/', 'ProductController@index');
                Route::get('/{product}', 'ProductController@show');
                Route::get('/{product}/free_categories', 'ProductController@freeCategories');
                Route::get('/{product}/comments', 'CommentsController@index');
                Route::get('/{product}/filters', 'ProductController@characteristics');
                Route::post('/{product}/filters', 'ProductController@postCharacteristic');
                Route::delete('/{product}/filters/{filter}', 'ProductController@deleteCharacteristic');
                Route::post('/', 'ProductController@create');
                Route::post('/{product}', 'ProductController@update');
                Route::delete('/{product}', 'ProductController@delete');
                Route::get('/{product}/checked_parts', 'ProductController@getProductItems');
                Route::post('/{product}/checked_parts', 'ProductController@postProductParts');
                Route::get('/{product}/checked_categories', 'ProductController@getProductItems');
                Route::post('/{product}/checked_categories', 'ProductController@postProductCategories');
                Route::get('/{product}/checked_filters', 'ProductController@getProductItems');
                Route::post('/{product}/checked_filters', 'ProductController@postProductFilters'); 
                Route::get('/{product}/same_products', 'ProductController@getSameProducts');
                Route::post('/{product}/same_products', 'ProductController@postSameProducts');
                Route::delete('/{product}/same_products/{same_product}', 'ProductController@deleteSameProducts');
                Route::delete('/promotions/{product}', 'ProductController@deletePromotion');
                Route::get('/{id}/chapters', 'ProductController@productChapters');   
                Route::post('/{product}/chapters/{chapter}', 'ProductController@postChapters');
                Route::delete('/{product}/chapters/{chapter}', 'ProductController@deleteChapters');   
                Route::get('/{product}/parts', 'ProductController@productParts');   
                Route::post('/{product}/category/{category}', 'ProductController@postParts');
                Route::delete('/{product}/category/{category}', 'ProductController@deleteParts');
                Route::get('/{product}/filters', 'ProductController@productFilters');   
                Route::post('/{product}/filters/{filter}', 'ProductController@postFilters');
                Route::delete('/{product}/filters/{filter}', 'ProductController@deleteFilters');
            }); 
            Route::post('/comments/{comment}', 'CommentsController@update');
            Route::group(['prefix' => 'news'], function () {
                Route::get('/', 'NewsController@index');
                Route::get('/{news}', 'NewsController@show');
                Route::post('/', 'NewsController@create');
                Route::post('/{news}', 'NewsController@update');
                Route::delete('/{news}', 'NewsController@delete');
            });
            Route::group(['prefix' => 'filters'], function () {
                Route::get('/characteristics', 'FiltersController@allCharacteristics');
                Route::get('/', 'FiltersController@index');
                Route::get('/{filter}', 'FiltersController@show');
                Route::post('/', 'FiltersController@saveData');
                Route::post('/{filter}', 'FiltersController@update');
                Route::delete('/{filter}', 'FiltersController@delete');
            });
            Route::group(['prefix' => 'recommended'], function () {
                Route::get('/', 'RecommendedController@index');
                Route::post('/{recommended}', 'ProductController@update');
            });            
            Route::group(['prefix' => 'new_books'], function () {
                Route::get('/', 'NewBookController@index');
                Route::post('/{new_books}', 'ProductController@update');
            }); 
            Route::group(['prefix' => 'characteristics'], function () {
                Route::get('/', 'CharacteristicsController@index');
                Route::get('/{characteristic}', 'CharacteristicsController@show');
                Route::post('/', 'CharacteristicsController@saveData');
                Route::post('/{characteristic}', 'CharacteristicsController@update');
                Route::delete('/{characteristic}', 'CharacteristicsController@delete');
            });
            Route::get('characteristic_types', function () {
                return CharacteristicType::all();
            });
            Route::get('innovations/{partId?}', 'InnovationController@getLinksByPartId'); 
            Route::get('innovations/free/{partId?}', 'InnovationController@getfreeLinksByPartId');  
            Route::post('innovations/{partId?}', 'InnovationController@addProductToInnovations');   
            Route::delete('innovations/{partId?}/product/{productId}', 'InnovationController@deleteProductToInnovations');   
            Route::post('product_images/{product}', 'ProductController@postImage');
            Route::post('news_images/{article}', 'NewsController@postImage');
            Route::get('banners/{id}', 'BannerController@getBannersByPartId');
            Route::post('banners/{id}', 'BannerController@saveBanners');
            Route::delete('banners/{id}', 'BannerController@deleteBanner');
            Route::get('promotions', 'PromotionController@index');
            Route::delete('promotions/{id}', 'PromotionController@deleteLink');
            Route::post('promotions/{id}', 'PromotionController@addLink');
            Route::post('audios/{product}', 'ProductController@postAudio');
            Route::delete('audios/{audio}', 'ProductController@deleteAudio');
            Route::delete('product-preview/{product}', 'ProductController@deletePreview');
        });

        Route::any('{all?}', function () {
            return view('admin.main');
        })->where(['all' => '.*']);
    });
});

Route::group(['namespace' => 'Index'], function () {
    Route::get('/', 'HomeController@index');
    Route::get('/catalog/{category}', 'HomeController@categories');
    Route::get('/dlia-dosuga',  function () { 
        return redirect('catalog/detskaya'); 
    });
    Route::get('/banners/{slug}', 'HomeController@banners');
    Route::group(['prefix' => 'books'], function () {
        Route::get('/promotions', 'BookController@promotions');
        Route::get('/recommended', 'BookController@recommended');
        Route::get('/new/{part}', 'BookController@new_books');
        Route::get('/all', 'BookController@allBooks'); 
        Route::get('/part/{slug}', 'BookController@booksByCharacteristicType');  
        Route::get('/{id}/same_books', 'BookController@sameBooks'); 
    });
    Route::group(['prefix' => 'news'], function () {
        Route::get('/', 'NewsController@index');
        Route::get('/{page}', 'NewsController@show');
    });
    Route::group(['prefix' => 'book'], function () {
        Route::get('/{page}', 'BookController@show');
        Route::get('/byId/{id}', 'BookController@getById');
    });
    Route::get('/contacts', function () {
        return view('index.contacts');
    });
    Route::get('/cart', function () {
        return view('index.cart');
    });
    Route::get('/order', function () {
        return view('index.order');
    });
    Route::get('/price/nachalnaya-shkola', 'HomeController@price');
});

