<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\CharacteristicType;

class AddCharacteristicTypes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        CharacteristicType::create([
            'name' => 'Шелковый путь',
            'slug' => 'shelkovyj-put'
        ]);
        CharacteristicType::create([
            'name' => 'Средняя школа',
            'slug' => 'srednjaja-shkola'
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
}
