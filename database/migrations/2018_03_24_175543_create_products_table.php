<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Schema::dropIfExists('products');
        \DB::statement('SET FOREIGN_KEY_CHECKS = 1');   
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 150);
            $table->string('image');
            $table->string('author', 150);
            $table->longtext('about_book', 3000)->nullable();
            $table->boolean('is_promotion');
            $table->boolean('is_new');
            $table->boolean('is_recommended');
            $table->string('isbn')->nullable()->default('');
            $table->string('book_id')->nullable()->default('');
            $table->string('page_number')->nullable()->default('');
            $table->string('cover_type')->nullable()->default('');
            $table->string('decor')->nullable()->default('');
            $table->string('illustration')->nullable()->default('');
            $table->string('video')->nullable()->default('');
            $table->string('weight')->nullable()->default('');
            $table->string('width')->nullable()->default('');
            $table->string('length')->nullable()->default('');
            $table->string('thickness')->nullable()->default('');
            $table->string('additional_info')->nullable()->default('');
            $table->string('year')->nullable()->default('');
            // $table->integer('characteristic_type_id')->unsigned();
            $table->integer('price');
            $table->timestamps();
        });
        // Schema::table('products', function ($table)
        // {
        //     $table->foreign('characteristic_type_id')->references('id')->on('characteristic_types');
        // });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Schema::dropIfExists('products');
        \DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Schema::dropIfExists('products');
        \DB::statement('SET FOREIGN_KEY_CHECKS = 1');        
    }
}
