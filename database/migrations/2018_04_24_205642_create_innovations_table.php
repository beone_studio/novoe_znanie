<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInnovationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('innovations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('link_product_part_id')->unsigned();
            $table->integer('order')->unsigned();
            $table->timestamps();
        });

        Schema::table('innovations', function ($table) {
            $table->foreign('link_product_part_id')->references('id')->on('links_product_parts')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('innovations', function (Blueprint $table) {
            $table->dropForeign(['link_product_part_id']);
        });

        Schema::dropIfExists('innovations');
    }
}
