<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewsImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('article_images', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('news_id')->unsigned();
            $table->string('image');
            $table->timestamps();
        });

        Schema::table('article_images', function ($table) {
            $table->foreign('news_id')->references('id')->on('articles')->onDelete('cascade');
        });     
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('article_images', function ($table) {
            $table->dropForeign(['news_id']);
        });  

        Schema::dropIfExists('article_images');
    }
}
