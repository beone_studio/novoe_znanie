<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLinksProductPartsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('links_product_parts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('product_id')->unsigned();
            $table->integer('part_id')->unsigned()->nullable();
            $table->timestamps();
        });

        Schema::table('links_product_parts', function ($table) {
            $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
            $table->foreign('part_id')->references('id')->on('characteristic_types')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('links_product_parts', function (Blueprint $table) {
            $table->dropForeign(['product_id']);
            $table->dropForeign(['part_id']);
        });

        Schema::dropIfExists('links_product_parts');
    }
}
