<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\CharacteristicType;

class CreateCharacteristicTypes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Schema::dropIfExists('characteristic_types');
        \DB::statement('SET FOREIGN_KEY_CHECKS = 1'); 
        Schema::create('characteristic_types', function (Blueprint $table) 
        {
            $table->increments('id');
            $table->string('name', 150);
            $table->string('slug', 150);
            $table->timestamps();
        });

        CharacteristicType::create([
            'name' => 'Детская литература',
            'slug' => 'detskaya'
        ]);
        CharacteristicType::create([
            'name' => 'Начальная школа',
            'slug' => 'nachalnaya-shkola'
        ]);
        CharacteristicType::create([
            'name' => 'Абитуриентам',
            'slug' => 'abiturientam'
        ]);
        CharacteristicType::create([
            'name' => 'Высшая школа',
            'slug' => 'vysshaya-shkola'
        ]);
        CharacteristicType::create([
            'name' => 'Медицинская литература',
            'slug' => 'medicina'
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Schema::dropIfExists('characteristic_types');
        \DB::statement('SET FOREIGN_KEY_CHECKS = 1');         
    }
}
