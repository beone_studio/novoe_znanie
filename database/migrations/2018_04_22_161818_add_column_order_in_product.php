<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnOrderInProduct extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->integer('order_new')->after('is_new')->unsigned();
            $table->integer('order_recommended')->after('is_recommended')->unsigned();
            $table->integer('order_promotions')->after('is_promotion')->unsigned();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->dropColumn('order_promotions');
            $table->dropColumn('order_recommended');
            $table->dropColumn('order_new');
        });     
    }
}
