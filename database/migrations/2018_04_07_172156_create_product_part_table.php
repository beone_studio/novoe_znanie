<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductPartTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Schema::dropIfExists('product_part');
        \DB::statement('SET FOREIGN_KEY_CHECKS = 1');
        Schema::create('product_part', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('product_id')->unsigned();
            $table->integer('characteristic_type_id')->unsigned();
            $table->integer('characteristic_id')->unsigned()->nullable();
            $table->integer('filter_id')->unsigned()->nullable();
            $table->timestamps();
        });
        Schema::table('product_part', function ($table) {
            $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
            $table->foreign('characteristic_type_id')->references('id')->on('characteristic_types')->onDelete('cascade');
            $table->foreign('characteristic_id')->references('id')->on('characteristics')->onDelete('set null');
            $table->foreign('filter_id')->references('id')->on('filters')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Schema::dropIfExists('product_part');
        \DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}
