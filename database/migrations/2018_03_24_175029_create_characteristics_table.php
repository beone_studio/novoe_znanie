<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCharacteristicsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Schema::dropIfExists('characteristics');
        \DB::statement('SET FOREIGN_KEY_CHECKS = 1'); 
        Schema::create('characteristics', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 150);
            $table->integer('characteristic_type_id')->unsigned();
            $table->string('slug')->unique();
            $table->tinyInteger('visible');
            $table->timestamps();
        });
        Schema::table('characteristics', function ($table) {
            $table->foreign('characteristic_type_id')->references('id')->on('characteristic_types')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Schema::dropIfExists('characteristics');
        \DB::statement('SET FOREIGN_KEY_CHECKS = 1');           
    }
}
