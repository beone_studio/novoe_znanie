<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use App\Banner;

class SeedTenBanners extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Banner::create([
            'id' => 1,
            'part_id' => 1,
        ]);
        Banner::create([
            'id' => 2,
            'part_id' => 1,
        ]);        
        Banner::create([
            'id' => 3,
            'part_id' => 2,
        ]);
        Banner::create([
            'id' => 4,
            'part_id' => 2,
        ]);
        Banner::create([
            'id' => 5,
            'part_id' => 3,
        ]);
        Banner::create([
            'id' => 6,
            'part_id' => 3,
        ]);
        Banner::create([
            'id' => 7,
            'part_id' => 4,
        ]);
        Banner::create([
            'id' => 8,
            'part_id' => 4,
        ]);
        Banner::create([
            'id' => 9,
            'part_id' => 5,
        ]);
        Banner::create([
            'id' => 10,
            'part_id' => 5,
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Banner::where('id', 1)->delete();
        Banner::where('id', 2)->delete();
        Banner::where('id', 3)->delete();
        Banner::where('id', 4)->delete();
        Banner::where('id', 5)->delete();
        Banner::where('id', 6)->delete();
        Banner::where('id', 7)->delete();
        Banner::where('id', 8)->delete();
        Banner::where('id', 9)->delete();
        Banner::where('id', 10)->delete();      
    }
}
