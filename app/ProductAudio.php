<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductAudio extends Model
{
    protected $fillable = [
    	'product_id',
    	'link',
    	'alias'
    ];

    public function product()
    {
    	return $this->belongsTo(Product::class);
    }
}
