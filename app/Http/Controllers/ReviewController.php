<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;
use App\Comment;

class ReviewController extends Controller
{

    public function postReview(Request $request){
        $data = [
        	'product_id' => $request->input('product_id'),
        	'user_name' => $request->input('user_name'),
        	'contacts' => $request->input('contacts'),
        	'review' => $request->input('review'),
        	'visible' => false,
        ];

        \DB::table('comments')->insert($data);

        return redirect(url(URL::previous()))->with('flash_message', 'Ваш отзыв отправлен на модерацию. Спасибо за Ваше мнение.');

    }
}
