<?php

namespace App\Http\Controllers\Admin;

use App\Comment;
use App\Product;
use Illuminate\Http\Request;
use Illuminate\Database\Query\Builder;
use App\Http\Controllers\Controller;

class CommentsController extends Controller
{
	public function index(Product $product) {
		$comments = Comment::where('product_id', $product->id)->get();
		return compact('comments');
	}

	public function update($id, Request $request) {
		$comment = Comment::where('id', $id)->first();
		$visible = $request->input('visible') === 'true' ? true : false;
		$data = [
			'name' => $request->input('name'),
			'product_id' => $request->input('product_id'), 
			'text' => $request->input('text'), 
			'visible' => $visible
		];
		$comment->update($data);
		return ['result' => $request->all()];
	}
}