<?php

namespace App\Http\Controllers\Admin;

use App\Characteristic;
use App\Filter;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FiltersController extends Controller
{
	public function index(Request $request) 
	{

        return Filter::get();
	}

	public function show(Filter $filter)
	{
		return $filter;
	}

	public function allCharacteristics()
	{
		return Characteristic::get();
	}


	public function loadForm(Request $request) 
	{
		$rules = Filter::rules();
		$validator = \Validator::make($request->all(), $rules);
		if($validator->fails()) {
			return [
				'status' => false,
				'errors' => $validator->failed()
			];
		}
		return [
			'status' => true
		];
	}

	public function save(Request $request) 
	{
		return Filter::create([
			'name' => $request->input('name'),
			'characteristic_id' => $request->input('characteristic_id'),
		]);
	}

	public function createErrorResponse($errors) 
	{
		$failMessage = '';
		foreach($errors as $key => $value) {
			$failMessage = $failMessage . $key . ' is required.' . "\n";
		}	
		return $failMessage;	
	}

	public function saveData(Request $request) 
	{
		$isLoaded = $this->loadForm($request);
		if($isLoaded['status'] && $this->save($request)) {
			return response([
				'success' => 'Фильтр ' . $request->input('name') . ' успешно создан',
				'route' => '/filters'
			], 200);
		}			
		$failMessage = $this->createErrorResponse($isLoaded['errors']);
		return response([
			'error_message' => $failMessage
		], 400);
	}

	public function delete($id)
    {
        $filter = Filter::getById($id);
        if($filter->delete()) {
        	return response([
				'success' => 'Фильтр ' . $filter->name . ' успешно удалён',
			], 200);
        } else {
        	return response([
				'error' => 'Фильтр ' . $filter->name . ' не может быть удалён',
			], 500);
        }
    }

    public function update(Request $request, $id)
    {
    	$filter = Filter::getById($id);
    	$data = [
			'name' => $request->input('name'),
			'characteristic_id' => $request->input('characteristic_id'),
    	];
    	if ($filter->update($data)) {
			return response([
				'success' => 'Фильтр ' . $request->input('name') . ' успешно обновлён',
				'route' => '/filters'
			], 200);    		
    	} else {
    		$failMessage = $this->createErrorResponse($isLoaded['errors']);
			return response([
				'error_message' => $failMessage
			], 400);    		
    	}
    }
}