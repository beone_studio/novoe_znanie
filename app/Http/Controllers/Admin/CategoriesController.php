<?php 

namespace App\Http\Controllers\Admin;

use App\Category;

use Illuminate\Http\Request;
use Illuminate\Database\Query\Builder;
use App\Http\Controllers\Controller;

class CategoriesController extends Controller 
{
    public function getSubCategories(Category $category) {
        $parent_id = $category->id;      
        $children = Category::subCategories($parent_id);
        $freeCategories = Category::freeCategories($parent_id);
        return compact('category', 'children', 'freeCategories');
    }

    public function getAllCategories() {
        return Category::get();
    }

    public function loadForm(Request $request) 
    {
        $rules = Category::rules();
        $validator = \Validator::make($request->all(), $rules);
        if($validator->fails()) {
            return [
                'status' => false,
                'errors' => $validator->failed()
            ];
        }
        return [
            'status' => true
        ];
    }

    public function save(Request $request) 
    {
        return Category::create([
            'name' => $request->input('name'),
            'slug' => $request->input('slug'),
            'parent_id' => $request->input('parent_id')
        ]);
    } 

    public function createErrorResponse($errors) 
    {
        $failMessage = '';
        foreach($errors as $key => $value) {
            $failMessage = $failMessage . $key . ' is required.' . "\n";
        }   
        return $failMessage;    
    }           

	public function create(Request $request) {
        $isLoaded = $this->loadForm($request);
        if($isLoaded['status'] && $this->save($request)) {
            return response([
                'success' => 'Категория ' . $request->input('name') . ' успешно создана',
                'route' => '/categories'
            ], 200);
        }           
        $failMessage = $this->createErrorResponse($isLoaded['errors']);
        return response([
            'error_message' => $failMessage
        ], 400);        
	}

	public function update(Category $category, Request $request)
    {
        $category = Category::getById($category->id);
        $data = [
            'name' => $request->input('name'),
            'slug' => $request->input('slug'),
            'parent_id' => $request->input('parent_id')     
        ];
        if ($category->update($data)) {
            return response([
                'success' => 'Категория ' . $request->input('name') . ' успешно обновлёна',
                'route' => '/categories'
            ], 200);            
        } else {
            $failMessage = $this->createErrorResponse($isLoaded['errors']);
            return response([
                'error_message' => $failMessage
            ], 400);            
        }        
    }	

	public function delete($id)
    {
        $category = Category::getById($id);
        if($category->delete()) {
            return response([
                'success' => 'Категория ' . $category->name . ' успешно удалёна',
            ], 200);
        } else {
            return response([
                'error' => 'Категория ' . $category->name . ' не может быть удалёна',
            ], 500);
        }        
    }	
    
}