<?php

namespace App\Http\Controllers\Admin;

use App\Product;
use App\Promotion;
use Illuminate\Http\Request;
use Illuminate\Database\Query\Builder;
use App\Http\Controllers\Controller;

class RecommendedController extends Controller
{
	public function index() {
		return Product::orderBy('is_recommended', 'DESC')->get();
	}

}