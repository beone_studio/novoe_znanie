<?php

namespace App\Http\Controllers\Admin;

use App\ProductImage;
use App\ProductAudio;
use App\LinksProductPart;
use App\Product;
use App\Category;
use App\Filter;
use App\Characteristic;
use App\CharacteristicType;
use Illuminate\Http\Request;
use Illuminate\Database\Query\Builder;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;

class ProductController extends Controller
{
    public function index(Request $request)
	{
        $filter = $request->get('name') ? '%' . $request->get('name') . '%' : null;
        $isbn = $request->get('isbn') ? '%' . $request->get('isbn') . '%' : null;
        $products = Product::filterByName($filter, $isbn);
        return compact('products');
	}

	public function show(Product $product) 
    {
        $images = $product->productImages()->get();
        $audios = $product->audios()->get();
        $product = array_merge($product->toArray(), array('categories' => $product->category));
		return compact('product', 'images', 'audios');
	} 

    public function freeCategories(Product $product) 
    {
        $product_id = $product->id;
        $freeCategories = Category::getFreeCategoriesForCurrentProduct($product_id);     
        return $freeCategories->toArray();
    }

    public function categoryAdding($id, Request $request) 
    {
        $product = Product::getById($id);
        $category_id = $request->input('category_id');
        $product->category()->attach($category_id);
        return ['result' => $category_id];
    }

    public function loadForm(Request $request) 
    {
        $rules = Product::rules();
        $validator = \Validator::make($request->all(), $rules);
        if($validator->fails()) {
            return [
                'status' => false,
                'errors' => $validator->failed()
            ];
        }
        return [
            'status' => true
        ];
    }

    public function save(Request $request) 
    {
        $product = Product::create([
            'name' => $request->input('name'),
            'slug' => $request->input('slug'),
            'image' => $request->image->store('products', 'public'),
            'about_book' => $request->input('about_book'),
            'author' => $request->input('author'),         
            'isbn' => $request->input('isbn'),
            'cover_type' => $request->input('cover_type'),
            'page_number' => $request->input('page_number'),
            'decor' => $request->input('decor'),
            'illustration' => $request->input('illustration'),
            'weight' => $request->input('weight'),
            'length' => $request->input('length'),
            'width' => $request->input('width'),
            'thickness' => $request->input('thickness'),
            'price' => $request->input('price'),
            'book_id' => $request->input('book_id'),
            'video' => $request->input('video'),
            'additional_info' => $request->input('additional_info'),
            'year' => $request->input('year'),
            'pdf_url_service' => $request->input('pdf_url_service'),
            // 'preview' => $request->preview->store('products/previews', 'public'),
        ]);

        if ($request->hasFile('preview')) {
            $product['preview'] = $request->preview->store('products/previews', 'public');
        }

        $product->save();

        return $product;
    }

    public function createErrorResponse($errors) 
    {
        $failMessage = '';
        foreach($errors as $key => $value) {
            $failMessage = $failMessage . $key . ' is required.' . "\n";
        }   
        return $failMessage;    
    }    

	public function create(Request $request) 
    {
        $isLoaded = $this->loadForm($request);
        if($isLoaded['status'] && $this->save($request)) {
            return response([
                'success' => 'Книга ' . $request->input('name') . ' успешно создана',
                'route' => '/products'
            ], 200);
        }           
        $failMessage = $this->createErrorResponse($isLoaded['errors']);
        return response([
            'error_message' => $failMessage
        ], 400);        
	}

	public function update($id, Request $request)
    {
        $product = Product::getById($id);
        $data = [
            'name' => $request->input('name'),
            'slug' => $request->input('slug'),
            'about_book' => $request->input('about_book') === 'null' ? '' : $request->input('about_book'),
            'author' => $request->input('author'),
            'isbn' => $request->input('isbn') === 'null' ? '' : $request->input('isbn'),
            'cover_type' => $request->input('cover_type') === 'null' ? '' : $request->input('cover_type'),
            'page_number' => $request->input('page_number') === 'null' ? '' : $request->input('page_number'),
            'decor' => $request->input('decor') === 'null' ? '' : $request->input('decor'),
            'illustration' => $request->input('illustration') === 'null' ? '' : $request->input('illustration'),
            'weight' => $request->input('weight') === 'null' ? '' : $request->input('weight'),
            'length' => $request->input('length') === 'null' ? '' : $request->input('length'),
            'width' => $request->input('width') === 'null' ? '' : $request->input('width'),
            'thickness' => $request->input('thickness') === 'null' ? '' : $request->input('thickness'),
            'price' => $request->input('price'),
            'book_id' => $request->input('book_id') === 'null' ? '' : $request->input('book_id'),
            'video' => $request->input('video') === 'null' ? '' : $request->input('video'),
            'additional_info' => $request->input('additional_info') === 'null' ? '' : $request->input('additional_info'),
            'year' => $request->input('year') === 'null' ? '' : $request->input('year'),
            'pdf_url_service' => $request->input('pdf_url_service') === 'null' ? '' : $request->input('pdf_url_service'),
            // 'characteristic_type_id' => $request->input('characteristic_type_id')       
        ];
        if ($request->hasFile('image')) {
            \Storage::disk('public')->delete($product->image);
            $data['image'] = $request->image->store('products', 'public');
        }
        if ($request->hasFile('preview')) {
            \Storage::disk('public')->delete($product->preview);
            $data['preview'] = $request->preview->store('products/previews', 'public');
        }        
        if ($product->update($data)) {
            return response([
                'success' => 'Книга ' . $request->input('name') . ' успешно обновлена',
                'route' => '/products'
            ], 200);            
        } else {
            $failMessage = $this->createErrorResponse($isLoaded['errors']);
            return response([
                'error_message' => $failMessage
            ], 400);            
        }        
    }

    public function delete($id)
    {
        $product = Product::getById($id);
        if($product->delete()) {
            return response([
                'success' => 'Книга ' . $product->name . ' успешно удалёна',
            ], 200);
        } else {
            return response([
                'error' => 'Книга ' . $product->name . ' не может быть удалёна',
            ], 500);
        }        
    }

    public function getSameProducts($id)
    {
        $sameProducts = Product::
            select('products.*')
            ->leftJoin('product_series', function ($join) {
                $join->on('products.id', '=', 'product_series.same_product_id');
            })
            ->where('product_series.product_id', $id)
            ->get();
        $freeSameProducts = Product::getFreeSameProducts($id);
        return compact('sameProducts', 'freeSameProducts');
    }   

    public function postSameProducts(Request $request, $id)
    {
        $product = Product::getById($id);
        $same_product_id = $request->input('same_product_id');
        $product->relatedSameProducts()->attach($same_product_id);
        return ['result' => 'success'];        
    }  

    public function deleteSameProducts($id, $same_product_id)
    {
        $product = Product::getById($id);
        $product->relatedSameProducts()->detach($same_product_id);
        return ['result' => 'success'];        
    }        	

    public function characteristics($id)
    {
        $filters = Product::where('id', $id)
            ->with('filters')->select('products.id', 'products.name')->get();
        $freeFilters = Filter::getFreeFiltersForCurrentProduct($id);
        return compact('filters', 'freeFilters');
    }  

    public function postCharacteristic(Request $request, $id)
    {
        $product = Product::getById($id);
        $filter_id = $request->input('filter_id');
        $product->filters()->attach($filter_id);
        return ['result' => 'success'];        
    }

    public function deleteCharacteristic($id, FIlter $filter)
    {
        $product = Product::getById($id);
        $product->filters()->detach($filter->id);
        return ['result' => 'success'];        
    }  

    public function deleteCategory(Product $product, Category $category, Request $request) {
        $product->category()->detach($category->id);
        return ['result' => 'success'];
    }

    public function getProductItems($productId, Request $request)
    {
        $tab = explode('/', $request->path());
        $tab = end($tab);

        $checkedItems = [];
        $freeItems = [];
        $list = [];

        $checked_parts = json_decode(json_encode(CharacteristicType::getChosenCharacteristicTypes($productId)), true);

        $column = array_column($checked_parts, 'id');

        $characteristicIds = Characteristic::leftJoin('product_part', function($join) use ($productId){
                $join->on('characteristics.id', '=', 'product_part.characteristic_id');
                $join->on('product_part.product_id', '=', \DB::raw("'" . $productId . "'"));
            })->select('characteristics.id as id', 'characteristics.name as name')
            ->where('product_part.product_id', $productId)
            ->groupBy('id')
            ->whereIn('characteristics.characteristic_type_id', $column)->get();

        $characteristicIds = array_column(json_decode(json_encode($characteristicIds), true), 'characteristic_id');
        
        $filterIds = array_column(json_decode(json_encode(\DB::table('product_part')->where('product_id', $productId)->get()), true), 'filter_id');  

        switch ($tab) {
            case 'checked_parts':
                $checkedItems = CharacteristicType::getChosenCharacteristicTypes($productId);
                $freeItems = CharacteristicType::getFreeCharacteristicTypes($productId);                
                break;
            case 'checked_categories':   
            // $checkedItems = Characteristic::leftJoin('product_part', function($join) use ($productId){
            //     $join->on('characteristics.id', '=', 'product_part.characteristic_id');
            //     $join->on('product_part.product_id', '=', \DB::raw("'" . $productId . "'"));
            // })->select('characteristics.id as id', 'characteristics.name as name')
            // ->where('product_part.product_id', $productId)
            // ->groupBy('id')
            // ->whereIn('characteristics.characteristic_type_id', $column)->get();
            // $freeItems = Characteristic::leftJoin('product_part', function($join) use ($productId){
            //     $join->on('characteristics.id', '=', 'product_part.characteristic_id');
            //     $join->on('product_part.product_id', '=', \DB::raw("'" . $productId . "'"));
            // })->select('characteristics.id as id', 'characteristics.name as name')
            // ->whereNull('product_part.product_id')
            // ->groupBy('id')
            // ->whereIn('characteristics.characteristic_type_id', $column)->get();
            // $list = CharacteristicType::join('product_part', function ($join) {
            //     $join()
            // })

                break; 
            case 'checked_filters':  
            $checkedItems = Filter::leftJoin('product_part', function($join) use ($productId){
                $join->on('filters.id', '=', 'product_part.filter_id');
                $join->on('product_part.product_id', '=', \DB::raw("'" . $productId . "'"));
            })->select('filters.id', 'filters.name')
            ->where('product_part.product_id', $productId)
            ->groupBy('filters.id')
            ->get();
            $freeItems = Filter::leftJoin('product_part', function($join) use ($productId){
                $join->on('filters.id', '=', 'product_part.filter_id');
                $join->on('product_part.product_id', '=', \DB::raw("'" . $productId . "'"));
            })->select('filters.id', 'filters.name')
            ->whereNull('product_part.product_id')
            ->groupBy('filters.id')
            ->get();                                                            
                break;                                
            
            default:
                # code...
                break;
        }

        return compact('checkedItems', 'freeItems', 'list');
    }

    public function postProductParts(Request $request, Product $product)
    {
        $checkedItems = json_decode($request->input('checkedItems'));
        $freeItems = json_decode($request->input('freeItems'));

        var_dump($checkedItems);
        die;
        foreach($checkedItems as $part) {
            $partId = $part->id;
            $hasPivot = Product::where('products.id', $product->id)->whereHas('characteristicTypes', function ($q) use ($partId) {
                $q->where('characteristic_types.id', $partId);
            })
            ->exists();    
            if ($hasPivot) {
                continue;
            } else {
                $product->characteristicTypes()->attach($partId);
                LinksProductPart::create([
                    'product_id' => $product->id,
                    'part_id' => $partId,
                ]);
            }
        }

        foreach($freeItems as $part) {
            $partId = $part->id;
            $hasPivot = Product::where('products.id', $product->id)->whereHas('characteristicTypes', function ($q) use ($partId) {
                $q->where('characteristic_types.id', $partId);
            })
            ->exists();    
            if ($hasPivot) {
                $product->characteristicTypes()->detach($partId);
                LinksProductPart::where('product_id', $product->id)->where('part_id', $partId)->first()->delete();
            } else {
                continue;
            }
        }

        return ['result' => 'success'];
    }

    public function postProductCategories(Request $request, Product $product)
    {
        $checkedItems = json_decode($request->input('checkedItems'));
        $freeItems = json_decode($request->input('freeItems')); 
        foreach($checkedItems as $category) {
            $obj = Characteristic::where('id', $category->id)->first();
            $part = CharacteristicType::where('id', $obj->characteristic_type_id)->first();
            // $partId = $obj->characteristic()->characteristicType();
            $rowWithoutFilter = \DB::table('product_part')
                ->where('product_id', $product->id)
                ->where('characteristic_type_id', $part->id)
                ->select('characteristic_id');
            if ($rowWithoutFilter->first()->characteristic_id === null) {
                $rowWithoutFilter->delete();
            }
            $data = [
                'product_id' => $product->id,
                'characteristic_type_id' => $part->id,
                'characteristic_id' => $category->id,
            ];
            if (\DB::table('product_part')
                    ->where('product_id', $data['product_id'])
                    ->where('characteristic_type_id', $data['characteristic_type_id'])
                    ->where('characteristic_id', $data['characteristic_id'])
                    ->first() === null) {
                        \DB::table('product_part')->insert($data);
                }
        }
    
        foreach($freeItems as $category) {
            $obj = Characteristic::where('id', $category->id)->first();
            $part = CharacteristicType::where('id', $obj->characteristic_type_id)->first();
            $rowWithCategory = \DB::table('product_part')
                ->where('product_id', $product->id)
                ->where('characteristic_type_id', $part->id)
                ->where('characteristic_id', $category->id);
            $rowWithCategory->delete();

            if(\DB::table('product_part')
                ->where('product_id', $product->id)
                ->where('characteristic_type_id', $part->id)
                ->first() === null
            ) {
                $data = [
                    'product_id' => $product->id,
                    'characteristic_type_id' => $part->id,
                ];

                \DB::table('product_part')->insert($data);
            }           
        }         

        return ['result' => 'success'];
    }

    public function postProductFilters(Request $request, Product $product)
    {
        $checkedItems = json_decode($request->input('checkedItems'));
        $freeItems = json_decode($request->input('freeItems')); 
        foreach($checkedItems as $filter) {
            $obj = Filter::where('id', $filter->id)->first();
            $category = Characteristic::where('id', $obj->characteristic_id)->first();
            $part = CharacteristicType::where('id', $category->characteristic_type_id)->first();
            // $partId = $obj->characteristic()->characteristicType();
            $rowWithoutFilter = \DB::table('product_part')
                ->where('product_id', $product->id)
                ->where('characteristic_id', $category->id)
                ->select('filter_id');
            if ($rowWithoutFilter->first()->filter_id === null) {
                $rowWithoutFilter->delete();
            }
            $data = [
                'product_id' => $product->id,
                'characteristic_type_id' => $part->id,
                'characteristic_id' => $category->id,
                'filter_id' => $obj->id,
            ];
            if (\DB::table('product_part')
                    ->where('product_id', $data['product_id'])
                    ->where('characteristic_type_id', $data['characteristic_type_id'])
                    ->where('characteristic_id', $data['characteristic_id'])
                    ->where('filter_id', $data['filter_id'])
                    ->first() === null) {
                        \DB::table('product_part')->insert($data);
                }
        }
    
        foreach($freeItems as $filter) {
            $obj = Filter::where('id', $filter->id)->first();
            $category = Characteristic::where('id', $obj->characteristic_id)->first();
            $part = CharacteristicType::where('id', $category->characteristic_type_id)->first();
            $rowWithCategory = \DB::table('product_part')
                ->where('product_id', $product->id)
                ->where('characteristic_type_id', $part->id)
                ->where('characteristic_id', $category->id)
                ->where('filter_id', $filter->id);
            $rowWithCategory->delete();

            if(\DB::table('product_part')
                ->where('product_id', $product->id)
                ->where('characteristic_type_id', $part->id)
                ->where('characteristic_id', $category->id)
                ->first() === null
            ) {
                $data = [
                    'product_id' => $product->id,
                    'characteristic_type_id' => $part->id,
                    'characteristic_id' => $category->id
                ];

                \DB::table('product_part')->insert($data);
            }           
        } 

        return ['result' => 'success'];
    }    

    public function deletePromotion($productId)
    {
        $product = Product::getById($productId);

        $product->is_promotion = $product->is_promotion ? false : true;
        $product->update();

        return ['result' => 'success'];
    }

    public function postImage(Request $request, Product $product)
    {
        $newProductImage = ProductImage::create([
            'product_id' => $product->id,
            'image' => $request->image->store('products', 'public'),
        ]);

        return $newProductImage->image_url;
    }

    public function postAudio(Request $request, Product $product)
    {
        $newAudio = ProductAudio::create([
            'product_id' => $product->id,
            'link' => $request->input('link'),
            'alias' => $request->input('alias'),
        ]);

        return ['result' => 'success'];
    }

    public function deleteAudio(Request $request, $audioId)
    {
        $audio = ProductAudio::where('id', $audioId)->first()->delete();

        return ['result' => 'success'];
    }  

    public function productChapters($productId)
    {
        $list = CharacteristicType::get();

        $checkedIds = \DB::table('product_part')->where('product_id', $productId)
            ->whereNotNull('characteristic_type_id')->select('characteristic_type_id')->groupBy('characteristic_type_id')->get();

        $checkedIds = array_column(json_decode(json_encode($checkedIds), true), 'characteristic_type_id');

        return compact('list', 'checkedIds');
    }   

    public function postChapters($productId, $chapterId)
    {
        $chapter = \DB::table('product_part')->where('product_id', $productId)->where('characteristic_type_id', $chapterId)->first();

        if ($chapter === null) {
            \DB::table('product_part')->insert([
                'product_id' => $productId,
                'characteristic_type_id' => $chapterId,
            ]);
        }

        $linksProductPart = \DB::table('links_product_parts')->where('product_id', $productId)->where('part_id', $chapterId)->get();

        if (\count($linksProductPart) === 0) {
            \DB::table('links_product_parts')->insert([
                'product_id' => $productId,
                'part_id' => $chapterId,
            ]);
        }

        return [
            'result' => 'success',
        ];
    }

    public function deleteChapters($productId, $chapterId)
    {
        $chapters = \DB::table('product_part')->where('product_id', $productId)->where('characteristic_type_id', $chapterId);

        $chapters->delete();

        $linksProductPart = \DB::table('links_product_parts')->where('product_id', $productId)->where('part_id', $chapterId);

        $linksProductPart->delete();        

        return [
            'result' => 'success',
        ];
    }     

    public function productParts($productId)
    {
        $list = CharacteristicType::join('product_part', function ($join) {
            $join->on('product_part.characteristic_type_id', '=', 'characteristic_types.id');
        })
        ->where('product_part.product_id', $productId)
        ->select('characteristic_types.id as id', 'characteristic_types.name as name')
        ->groupBy('characteristic_types.id')
        ->get();

        $partIds = array_column(json_decode(json_encode($list), true), 'id');

        $categories = Characteristic::whereIn('characteristic_type_id', $partIds)->get();

        $checkedIds = \DB::table('product_part')->where('product_id', $productId)
            ->whereNotNull('characteristic_id')->select('characteristic_id')->groupBy('characteristic_id')->get();

        $checkedIds = array_column(json_decode(json_encode($checkedIds), true), 'characteristic_id');

        return compact('list', 'categories', 'checkedIds');
    }  

    public function postParts($productId, $categoryId)
    {
        $category = Characteristic::where('id', $categoryId)->first();
        $chapterId = $category->characteristic_type_id;

        $chapters = \DB::table('product_part')->where('product_id', $productId)->where('characteristic_type_id', $chapterId)->whereNull('characteristic_id');

        $chapters->delete();   

        $category = \DB::table('product_part')->where('product_id', $productId)->where('characteristic_type_id', $chapterId)->where('characteristic_id', $categoryId)->first();

        if ($category === null) {
            \DB::table('product_part')->insert([
                'product_id' => $productId,
                'characteristic_type_id' => $chapterId,
                'characteristic_id' => $categoryId,
            ]);
        }

        return [
            'result' => 'success'
        ];        
    } 

    public function deleteParts($productId, $categoryId)
    {
        $category = Characteristic::where('id', $categoryId)->first();
        $chapterId = $category->characteristic_type_id;        

        $category = \DB::table('product_part')->where('product_id', $productId)->where('characteristic_type_id', $chapterId)->where('characteristic_id', $categoryId);

        $category->delete();

        $chapters = \DB::table('product_part')->where('product_id', $productId)->where('characteristic_type_id', $chapterId)->get();

        if (\count($chapters) === 0) {
            \DB::table('product_part')->insert([
                'product_id' => $productId,
                'characteristic_type_id' => $chapterId,
            ]);
        }

        return [
            'result' => 'success',
        ];
    }

    public function productFilters($productId)
    {
        $list = CharacteristicType::join('product_part', function ($join) {
            $join->on('product_part.characteristic_type_id', '=', 'characteristic_types.id');
        })
        ->where('product_part.product_id', $productId)
        ->select('characteristic_types.id as id', 'characteristic_types.name as name')
        ->groupBy('characteristic_types.id')
        ->get();        

        $listCategories = Characteristic::join('product_part', function ($join) {
            $join->on('product_part.characteristic_id', '=', 'characteristics.id');
        })
        ->where('product_part.product_id', $productId)
        ->select('characteristics.id as id', 'characteristics.name as name', 'characteristics.characteristic_type_id as type_id')
        ->groupBy('characteristics.id')
        ->get();

        $partIds = array_column(json_decode(json_encode($listCategories), true), 'id');

        $categories = Filter::whereIn('characteristic_id', $partIds)->get();

        $checkedIds = \DB::table('product_part')->where('product_id', $productId)
            ->whereNotNull('filter_id')->select('filter_id')->groupBy('filter_id')->get();

        $checkedIds = array_column(json_decode(json_encode($checkedIds), true), 'filter_id');

        return compact('list', 'listCategories', 'categories', 'checkedIds');
    }  

    public function postFilters($productId, $filterId)
    {
        $filter = Filter::where('id', $filterId)->first();
        $category = Characteristic::where('id', $filter->characteristic_id)->first();
        $categoryId = $category->id;
        $chapterId = $category->characteristic_type_id;

        $categories = \DB::table('product_part')->where('product_id', $productId)->where('characteristic_type_id', $chapterId)->where('characteristic_id', $categoryId)->whereNull('filter_id');

        $categories->delete();   

        $filter = \DB::table('product_part')->where('product_id', $productId)->where('characteristic_type_id', $chapterId)->where('characteristic_id', $categoryId)->where('filter_id', $filterId)->first();

        if ($filter === null) {
            \DB::table('product_part')->insert([
                'product_id' => $productId,
                'characteristic_type_id' => $chapterId,
                'characteristic_id' => $categoryId,
                'filter_id' => $filterId
            ]);
        }

        return [
            'result' => 'success'
        ];        
    } 

    public function deleteFilters($productId, $filterId)
    {
        $filter = Filter::where('id', $filterId)->first();
        $category = Characteristic::where('id', $filter->characteristic_id)->first();
        $categoryId = $category->id;
        $chapterId = $category->characteristic_type_id;        

        $filters = \DB::table('product_part')->where('product_id', $productId)->where('characteristic_type_id', $chapterId)->where('characteristic_id', $categoryId)->where('filter_id', $filterId);

        $filters->delete();

        $categories = \DB::table('product_part')->where('product_id', $productId)->where('characteristic_type_id', $chapterId)->where('characteristic_id', $categoryId)->get();

        if (\count($categories) === 0) {
            \DB::table('product_part')->insert([
                'product_id' => $productId,
                'characteristic_type_id' => $chapterId,
                'characteristic_id' => $categoryId,
            ]);
        }

        return [
            'result' => 'success',
        ];
    }  

    public function deletePreview($productId)
    {
        $product = Product::getById($productId);
        \Storage::disk('public')->delete($product->preview);

        $product->preview = '';

        $product->save();

        return ['result' => 'success'];
    }          
}