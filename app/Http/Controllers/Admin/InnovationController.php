<?php 

namespace App\Http\Controllers\Admin;

use App\Innovation;
use App\LinksProductPart;
use App\Product;

use Illuminate\Http\Request;
use Illuminate\Database\Query\Builder;
use App\Http\Controllers\Controller;

class InnovationController extends Controller
{
	public function getLinksByPartId($partId)
	{		
		return Innovation::getByPartId($partId);
	}

	public function getfreeLinksByPartId($partId)
	{
		return Innovation::getFreeByPartId($partId);
	}

	public function addProductToInnovations(Request $request, $partId)
	{
		if ($partId === '0') {
			$partId = null;
		}
		$productId = $request->input('product_id');
		$insertItemId = null;
		if ($partId === null) {
			$insertItemId = LinksProductPart::create([
				'product_id' => $productId,
				'part_id' => $partId,
			])->id;
		} else {
			$insertItemId = LinksProductPart::where('part_id', $partId)->where('product_id', $productId)->first()->id;
		}

		$order = Innovation::join('links_product_parts', function ($join) {
			$join->on('links_product_parts.id', '=', 'innovations.link_product_part_id');
		})
		->where('links_product_parts.part_id', $partId)
		->max('order');

		Innovation::create([
			'link_product_part_id' => $insertItemId,
			'order' => $order + 1,
		]);

		return ['result' => 'success'];
	}

	public function deleteProductToInnovations($partId, $productId)
	{
		if ($partId === '0') {
			$partId = null;
		}	
		$link = LinksProductPart::where('part_id', $partId)->where('product_id', $productId)->first();

		$innovation = Innovation::where('link_product_part_id', $link->id)->first();

		$innovation->delete();

		if ($partId === null) {
			$link->delete();
		}

		return ['result' => 'success'];	
	}
}