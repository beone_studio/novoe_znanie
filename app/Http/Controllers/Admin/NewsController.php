<?php

namespace App\Http\Controllers\Admin;

use App\NewsImage;
use App\Article;
use Illuminate\Http\Request;
use Illuminate\Database\Query\Builder;
use App\Http\Controllers\Controller;

class NewsController extends Controller
{ 

    public function index(Request $request)
	{
        $filter = '%' . $request->get('name') . '%';
        $news = Article::filterByName($filter);
        return compact('news');
	}

	public function show($id) {
        $images = Article::find($id)->images()->get();
		$article = Article::getById($id);
		return compact('article', 'images');
	} 

    public function loadForm(Request $request) 
    {
        $rules = Article::rules();
        $validator = \Validator::make($request->all(), $rules);
        if($validator->fails()) {
            return [
                'status' => false,
                'errors' => $validator->failed()
            ];
        }
        return [
            'status' => true
        ];
    }

    public function save(Request $request) 
    {
        return Article::create([
            'name' => $request->input('name'),
            'slug' => $request->input('slug'),
            'video1' => $request->input('video1'),
            'video2' => $request->input('video2'),
            'image' => $request->image->store('news', 'public'),
            'info' => $request->input('info')
        ]);
    }

    public function createErrorResponse($errors) 
    {
        $failMessage = '';
        foreach($errors as $key => $value) {
            $failMessage = $failMessage . $key . ' is required.' . "\n";
        }   
        return $failMessage;    
    }    

	public function create(Request $request) {
        $isLoaded = $this->loadForm($request);
        if($isLoaded['status'] && $this->save($request)) {
            return response([
                'success' => 'Новость ' . $request->input('name') . ' успешно создана',
                'route' => '/news'
            ], 200);
        }           
        $failMessage = $this->createErrorResponse($isLoaded['errors']);
        return response([
            'error_message' => $failMessage
        ], 400);        
	}

	public function update($id, Request $request)
    {
        $article = Article::getById($id);
        $data = [
            'name' => $request->input('name'),
            'slug' => $request->input('slug'),
            'video1' => $request->input('video1') === 'null' ? '' : $request->input('video1'),
            'video2' => $request->input('video2') === 'null' ? '' : $request->input('video2'),
            'info' => $request->input('info') === 'null' ? '' : $request->input('info'), 
        ];
        if ($article->update($data)) {
            return response([
                'success' => 'Новость ' . $request->input('name') . ' успешно обновлёна',
                'route' => '/news'
            ], 200);            
        } else {
            $failMessage = $this->createErrorResponse($isLoaded['errors']);
            return response([
                'error_message' => $failMessage
            ], 400);            
        }             
    }

    public function delete($id)
    {
        $article = Article::getById($id);
        if($article->delete()) {
            return response([
                'success' => 'Новость ' . $article->name . ' успешно удалёна',
            ], 200);
        } else {
            return response([
                'error' => 'Новость ' . $article->name . ' не может быть удалёна',
            ], 500);
        }        
    }

    public function postImage(Request $request, $newsId)
    {
        $newArticleImage = NewsImage::create([
            'news_id' => $newsId,
            'image' => $request->image->store('news', 'public'),
        ]);

        return $newArticleImage->image_url;
    }    	
}