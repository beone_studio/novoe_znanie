<?php 

namespace App\Http\Controllers\Admin;

use App\Promotion;
use App\Product;
use Illuminate\Http\Request;
use Illuminate\Database\Query\Builder;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;


class PromotionController extends Controller
{
	public function index()
	{
		return Product::with('promotion')->get();
	}

	public function deleteLink($productId)
	{
		$promotion = Promotion::where('product_id', $productId)->first();

		$promotion->delete();

		return ['result' => 'success'];
	}

	public function addLink($productId)
	{
		$order = Promotion::max('order') + 1;

		Promotion::create([
			'product_id' => $productId,
			'order' => $order,
		]);

		return ['result' => 'success'];
	}	
}