<?php

namespace App\Http\Controllers\Admin;

use Validator;
use App\Characteristic;
use App\Product;
use Illuminate\Http\Request;
use Illuminate\Database\Query\Builder;
use App\Http\Controllers\Controller;

class CharacteristicsController extends Controller
{
	public function index(Request $request) 
	{
		$filter = $request->get('name');
		return Characteristic::filterByName($filter);
	}

	public function show(Characteristic $characteristic)
	{
		return $characteristic;
	}


	public function loadForm(Request $request) 
	{
		$rules = Characteristic::rules();
		$validator = \Validator::make($request->all(), $rules);
		if($validator->fails()) {
			return [
				'status' => false,
				'errors' => $validator->failed()
			];
		}
		return [
			'status' => true
		];
	}

	public function save(Request $request) 
	{
		$visible = $request->input('visible') === 'true' ? 1 : 0;
		return Characteristic::create([
			'name' => $request->input('name'),
			'characteristic_type_id' => $request->input('characteristic_type_id'),
			'slug' => $request->input('slug'),
			'visible' => $visible,
		]);
	}

	public function createErrorResponse($errors) 
	{
		$failMessage = '';
		foreach($errors as $key => $value) {
			$failMessage = $failMessage . $key . ' is required.' . "\n";
		}	
		return $failMessage;	
	}

	public function saveData(Request $request) 
	{
		$isLoaded = $this->loadForm($request);
		if($isLoaded['status'] && $this->save($request)) {
			return response([
				'success' => 'Фильтр ' . $request->input('name') . ' успешно создан',
				'route' => '/characteristics'
			], 200);
		}			
		$failMessage = $this->createErrorResponse($isLoaded['errors']);
		return response([
			'error_message' => $failMessage
		], 400);
	}

	public function delete($id)
    {
        $characteristic = Characteristic::getById($id);
        if($characteristic->delete()) {
        	return response([
				'success' => 'Фильтр ' . $characteristic->name . ' успешно удалён',
			], 200);
        } else {
        	return response([
				'error' => 'Фильтр ' . $characteristic->name . ' не может быть удалён',
			], 500);
        }
    }

    public function update(Request $request, $id)
    {
    	$visible = $request->input('visible') === 'true' ? 1 : 0;
    	$characteristic = Characteristic::getById($id);
    	$data = [
			'name' => $request->input('name'),
			'characteristic_type_id' => $request->input('characteristic_type_id'),
			'slug' => $request->input('slug'),
			'visible' => $visible,    	
    	];
    	if ($characteristic->update($data)) {
			return response([
				'success' => 'Фильтр ' . $request->input('name') . ' успешно обновлён',
				'route' => '/characteristics'
			], 200);    		
    	} else {
    		$failMessage = $this->createErrorResponse($isLoaded['errors']);
			return response([
				'error_message' => $failMessage
			], 400);    		
    	}
    }
}