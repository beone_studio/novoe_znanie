<?php 

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Banner;

class BannerController extends Controller
{
	public function getBannersByPartId($partId)
	{
		$banners = Banner::where('part_id', $partId)->get();

		return $banners;
	}

	public function saveBanners(Request $request, $partId)
	{
		$bannerId = $request->input('id');
		$banner = Banner::where('id', $bannerId)->first();
		$banner['url'] = $request->input('url');

		if ($request->hasFile('image')) {
            \Storage::disk('public')->delete($banner->image);
            $banner['image'] = $request->image->store('banners', 'public');
        }

        $banner->update();

		return ['result' => 'success'];
	}

	public function deleteBanner($id) 
	{
		$banner = Banner::where('id', $id)->first();
		$banner['url'] = '/';
		\Storage::disk('public')->delete($banner->image);
		$banner['image'] = '';

		$banner->update();
	}
}