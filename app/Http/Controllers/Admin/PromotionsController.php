<?php

namespace App\Http\Controllers\Admin;

use App\Product;
use Illuminate\Http\Request;
use Illuminate\Database\Query\Builder;
use App\Http\Controllers\Controller;

class PromotionsController extends Controller
{
	public function index() {
		return Product::orderBy('is_promotion', 'DESC')->get();
	}

}