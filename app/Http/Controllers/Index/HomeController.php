<?php

namespace App\Http\Controllers\Index;

use App\Article;
use App\Banner;
use Illuminate\Http\Request;
use App\CharacteristicType;
use App\Characteristic;
use App\Product;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    public function index()
    {
        $news = Article::orderBy('created_at', 'DESC')->paginate(4);
        return view('index.main', compact('news'));
    }

    public function test() 
    {
        return view('index.catalog');
    }

    public function banners($slug)
    {
        $part = CharacteristicType::getBySlug($slug);
        $banners = Banner::where('part_id', $part->id)->get();

        return $banners;
    }


    function price() {
        $part = CharacteristicType::getBySlug('nachalnaya-shkola');
        $characteristics = Characteristic::getByCharacteristicTypeId($part->id);

        return  view('index.price', compact('characteristics', 'part'));
    }

    public function categories($slug, Request $request)
    {
        $query = $request->query();
        $part = CharacteristicType::getBySlug($slug);
        $characteristics = Characteristic::getByCharacteristicTypeId($part->id); 
        $filters = [];
        // $delimiter = ',';
        // foreach($query as $filter => $value) {
        //     $value = explode($delimiter, $value);
        //     $filters = array_merge($filters, $value);
        // }
        // $query = ['age' => '1', 'price' => '2']; 
        // $filters = [];
        // $part = CharacteristicType::getBySlug($slug);
        // $characteristics = Characteristic::getByCharacteristicTypeId($part->id);
        // foreach($query as $filterName => $filterIds) { 
        //   $filters[$filterName] = explode(',', $filterIds); 
        // }        

        return view('index.catalog', compact('characteristics', 'slug', 'part'));
    }
}
