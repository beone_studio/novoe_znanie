<?php

namespace App\Http\Controllers\Index;

use App\Product;
use App\ProductAudio;
use App\Promotion;
use App\CharacteristicType;
use App\Characteristic;
use Illuminate\Http\Request;
use App\Comment;
use App\Http\Controllers\Controller;

class BookController extends Controller {

	public function allBooks() 
	{
		return Product::allBooks();
	}

	public function promotions() 
	{
		return Product::join('promotions_table', function ($join) {
			$join->on('products.id', '=', 'promotions_table.product_id');
		})->select('products.id as id','products.name as name','products.price as price', 'products.author as author','products.image as image', 'products.slug as slug')->get();
	}

	public function new_books($partId) 
	{
		if ($partId === '0') {
			$partId = null;
		}
		// return Product::getNewBooks($partId);	
		return Product::join('links_product_parts', function ($join) {
			$join->on('links_product_parts.product_id', '=', 'products.id');	
		})	
		->where('part_id', $partId)
		->join('innovations', function ($join) {
			$join->on('innovations.link_product_part_id', '=', 'links_product_parts.id');
		})
		->orderBy('order', 'ASC')
		->select('products.id as id','products.name as name','products.price as price', 'products.author as author','products.image as image', 'products.slug as slug')
		->get();
	}

	public function recommended()
	{
		return Product::getRecommended();
	}	

	public function show($slug) 
	{
		$book = Product::where('slug', $slug)->first();
		$comments = Comment::where('product_id', $book->id)->where('visible', true)->get();
		$audios = ProductAudio::where('product_id', $book->id)->get();
		if($book) {
			return view('index.inner', compact('book', 'comments', 'audios'));
		} else {
			abort(404);
		}		
	}

    public function getById($id)
    {
        $book = Product::where('id', $id)->first();

        if($book) {
            return compact('book');
        } else {
            abort(404);
        }
    }

	public function booksByCharacteristicType($slug, Request $request)
	{
		$query = $request->query(); 
		$filters = [];
		$part = CharacteristicType::getBySlug($slug);
		foreach($query as $filterName => $filterIds) { 
	      $filters[$filterName] = explode(',', $filterIds); 
	    }

		$query = Product::join('product_part', function ($join) {
			$join->on('products.id', '=', 'product_part.product_id');
		})
		->where('characteristic_type_id', $part->id);
		$query->where(function ($query1) use ($filters) {
			foreach($filters as $categorySlug => $filterIds) {
				$query1->where(function ($query2) use ($filterIds) {
					$query2->whereIn('product_part.filter_id', $filterIds);
				});
			}
		});

		$books = $query->select('products.*')->groupBy('products.id')->orderBy('updated_at', 'desc')->get();

        return compact('books');		
	}

	public function sameBooks($slug)
	{
		$book = Product::where('slug', $slug)->first();

        return Product::
            select('products.*')
            ->leftJoin('product_series', function ($join) {
                $join->on('products.id', '=', 'product_series.same_product_id');
            })
			->where('product_series.product_id', $book->id)
			->orderBy('updated_at', 'desc')
            ->get();
	}
}