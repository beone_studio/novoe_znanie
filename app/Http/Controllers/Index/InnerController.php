<?php

namespace App\Http\Controllers\Index;

use App\Article;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class InnerController extends Controller {
	public function index() {
		return view('index.inner');
	}
}