<?php

namespace App\Http\Controllers\Index;

use App\Article;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class NewsController extends Controller {
	public function index() {
		$news = Article::getWithPagination();
		return view('index.news', compact('news'));
	}

	public function show($page) {
		$article = Article::getBySlug($page);
		if($article) {
			$last_four_news = Article::getLastNews($page);
			return view('index.news_inner', compact('article', 'last_four_news'));
		} else {
			abort(404);
		}		
	}
}