<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Notification;
use Illuminate\Support\Facades\URL;

class EmailController extends Controller
{
    public function sendOrder(Request $request) {
        $title = 'Новый заказ';
        $name = $request->input('name', '');
        $email = $request->input('email', '');
        $phone = $request->input('phone', '');
        $total= $request->input('total', '');
        $text = $request->input('notes', '');
        $items = $request->input('items', '');

        Mail::send(
            'emails.order',
            [
                'title' => $title,
                'name' => $name,
                'email' => $email,
                'phone' => $phone,
                'total' => $total,
                'text' => $text,
                'items' => $items
            ],
            function ($message) use ($title, $email)
            {
                $message->from('sale@wnk.biz', 'Новый заказ');
                $message->to('sale@wnk.biz')->subject('Новый заказ');
            }
        );

        return [ 'response' => 'OK'];
    }

    public function notifications(Request $request){
        $email = $request->input('mail');

        Mail::send(
            'emails.notifications',
            [
                'email' => $email,
            ],
            function ($message) use ($email)
            {

                $message->from($email, 'Подписка на рассылку');

                $message->to($email)->subject('Подписка на рассылку');

            }
        );

        if (Notification::where('email', $email)->first() === null) {
            Notification::create([
                'email' => $email,
            ]);

            return redirect(url(URL::previous()))->with('flash_message', 'Спасибо за подписку.');
        } else {
            return redirect(url(URL::previous()))->with('flash_message', 'Вы уже подписаны.');
        }
    }
}
