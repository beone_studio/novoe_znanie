<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
	protected $fillable = [
		'product_id',
		'user_name',
		'login',
		'review',
		'visible',
	];

	protected $casts = [
		'product_id' => 'integer',
		'user_name' => 'string',
		'login' => 'string',
		'review' => 'string',
		'visible' => 'boolean',
	];

	public static function rules(): array 
    {
        return [
            'product_id' => 'required|integer',
            'user_name' => 'required|max:150',
            'login' => 'unique:comments|required',
            'review' => 'required|max:3000',
            'visible' => 'boolean',
        ];
    } 	

    public function products()
    {
    	return $this->belongsTo(Product::class);
    }
}