<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Promotion extends Model
{
    protected $table = 'promotions_table';

    protected $fillable = [
    	'product_id',
    	'order',
    ];
}
