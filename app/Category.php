<?php

namespace App;

use App\Category;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
	protected $fillable = [
		'name',
		'slug',
		'parent_id',
	];

	protected $casts = [
		'name' => 'string',
		'slug' => 'string',
		'parent_id' => 'integer',
	];

	public static function rules(): array 
    {
        return [
            'name' => 'required|max:150',
            'slug' => 'unique:categories|required|max:150',
            'parent_id' => 'nullable',
        ];
    } 	

    public function parent()
    {
    	return $this->belongsTo(Category::class);
    }

    public function children()
    {
    	return $this->hasMany(Category::class, 'parent_id');
    }

    public function books()
    {
        return $this->belongsToMany(Product::class);
    }

    public function scopeSubCategories($query, $id)
    {
        return $query
            ->where('parent_id', $id)
            ->withCount('children')
            ->get();
    }

    public function scopeFreeCategories($query, $id)
    {
        return $query
            ->where([
                ['parent_id', '<>', $id],
                ['id', '<>', $id]
            ])
            ->get();
    }

    public function scopeGetById($query, $id)
    {
        return $query
            ->where('id', $id)
            ->first();
    }

    public function scopeGetFreeCategoriesForCurrentProduct($query, $productId)
    {
        return $query
            ->leftJoin('product_categories', function($join) use ($productId){
                $join->on('categories.id', '=', 'product_categories.category_id');
                $join->on('product_categories.product_id', '=', \DB::raw("'".$productId."'"));
            })->whereNull('product_categories.category_id')->select('categories.id as id', 'categories.name as name')->get();
    }
}