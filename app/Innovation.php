<?php

namespace App;
use App\Product;

use Illuminate\Database\Eloquent\Model;

class Innovation extends Model
{
    protected $fillable = [
    	'link_product_part_id',
    	'order',
    ];

    public function getLink()
    {
    	return $this->hasOne(LinksProductPart::class);
    }

    public function scopeGetByPartId($query, $partId)
    {
    	if ($partId === '0') {
    		$partId = null;
    	}
    	return $query
    		->join('links_product_parts', function ($join) {
    			$join->on('links_product_parts.id', '=', 'innovations.link_product_part_id');
    		})
    		->join('products', function ($join) {
    			$join->on('products.id', '=', 'links_product_parts.product_id');
    		})
    		->where('links_product_parts.part_id', $partId)
    		->select('*')
    		->orderBy('innovations.order')
    		->get();
    }

	public function scopeGetFreeByPartId($query, $partId)
    {
		if ($partId === '0') {
    		$partId = null;
            $freeProducts = \DB::table('products')
                ->join('links_product_parts', function ($join) {
                    $join->on('links_product_parts.product_id', '=', 'products.id');
                })
                ->leftJoin('innovations', function ($join) {
                    $join->on('innovations.link_product_part_id', '=', 'links_product_parts.id');
                })
                ->whereNull('innovations.link_product_part_id')->select('products.id as id', 'products.name as name')->groupBy('products.id')->get();            
    	} else {

            $freeProducts = \DB::table('products')
            ->join('links_product_parts', function ($join) {
                $join->on('links_product_parts.product_id', '=', 'products.id');
            })
            ->where('links_product_parts.part_id', $partId)
            ->leftJoin('innovations', function ($join) {
                $join->on('innovations.link_product_part_id', '=', 'links_product_parts.id');
            })
            ->whereNull('innovations.link_product_part_id')->select('products.id as id', 'products.name as name')->groupBy('products.id')->get();
        }

    	return $freeProducts;
    }    
}
