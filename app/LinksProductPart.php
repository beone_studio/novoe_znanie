<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LinksProductPart extends Model
{
    protected $fillable = [
    	'product_id',
    	'part_id',
    ];

    public function getProduct()
    {
    	return $this->hasOne(Product::class);
    }

    public function getPart()
    {
    	return $this->hasOne(CharacteristicType::class);
    }

    public function getInnovations()
    {
    	return $this->hasMany(Innovation::class);
    }
}
