<?php

namespace App;

use App\Product;
use App\CharacteristicType;
use Illuminate\Database\Eloquent\Model;

class Characteristic extends Model
{
    protected $fillable = [
        'name',
        'characteristic_type_id',
        'slug',
        'visible',
    ];

    protected $casts = [
        'name' => 'string',
        'characteristic_type_id' => 'integer',
        'slug' => 'string',
        'visible' => 'integer',
    ];


    public static function rules(): array 
    {
        return [
            'name' => 'required|max:150',
            'characteristic_type_id' => 'integer|required',
            'slug' => 'required'
        ];
    } 

    public function scopeFilterByName($query, $name) 
    {
        return $query->where('characteristics.name', 'like', '%' . $name . '%')
            ->join('characteristic_types', 'characteristics.characteristic_type_id', '=', 'characteristic_types.id')
            ->select('characteristics.id','characteristics.name', 'characteristic_types.name AS type_name', 'characteristics.visible')
            ->orderBy('characteristic_type_id', 'ASC')
            ->paginate(8);
    }

    public function scopeGetById($query, $id)
    {
        return $query
            ->where('id', $id)
            ->first();
    }

    public function characteristicType() 
    {
        return $this->belongsTo(CharacteristicType::class);
    }

    public function books()
    {
        return $this->belongsToMany(Product::class, 'product_part', 'characteristic_id', 'product_id');
    }    

    public function filters()
    {
        return $this->hasMany(Filter::class);
    }

    public function scopeGetFreeCharacteristicsForCurrentProduct($query, $productId)
    {
        return $query
            ->leftJoin('product_characteristics', function($join) use ($productId){
                $join->on('characteristics.id', '=', 'product_characteristics.characteristic_id');
                $join->on('product_characteristics.product_id', '=', \DB::raw("'".$productId."'"));
            })->whereNull('product_characteristics.characteristic_id')->select('characteristics.id as id', 'characteristics.name as name')->get();
    }  

    public function scopeGetByCharacteristicTypeId($query, $id)
    {
        return $query
            ->where('characteristic_type_id', $id)
            ->with('filters')
            ->groupBy('id')
            ->get();
    }   

    public function scopeGetChosenCategory($query, $productId)
    {
        $checked_parts = json_decode(json_encode(CharacteristicType::getChosenCharacteristicTypes($productId)), true);

        $column = array_column($checked_parts, 'id');

        $result = CharacteristicType::with('characteristics')->whereIn('id', $column)->get();


        return $result;
    }

    public function scopeGetFreeCategory($query, $productId)
    {
        $checked_parts = json_decode(json_encode(CharacteristicType::getChosenCharacteristicTypes($productId)), true);

        $column = array_column($checked_parts, 'id');

        $result = CharacteristicType::with('characteristics')->whereNotIn('id', $column)->get();       

        foreach($result as $i => $category) {
            if (empty($category->ch_name)) {
                $result[$i] = [];
            }
        }     

        return $result;        
    }  

    public function queryForChaining($query, $productId)
    {
        return $query
            ->leftJoin('product_part', function($join) use ($productId){
                $join->on('characteristics.id', '=', 'product_part.characteristic_id');
                $join->on('product_part.product_id', '=', \DB::raw("'" . $productId . "'"));
            })->select('characteristics.id as id', 'characteristics.name as name');
    }

    public function scopeGetChosenCharacteristics($query, $productId)
    {
        return $this
            ->queryForChaining($query, $productId)
            ->where('product_part.product_id', $productId)
            ->groupBy('id')
            ->get();
    }

    public function scopeGetFreeCharacteristics($query, $productId)
    {
        return $this
            ->queryForChaining($query, $productId)
            ->whereNull('product_part.characteristic_id')
            ->groupBy('id')
            ->get();
    }      
}
