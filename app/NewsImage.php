<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NewsImage extends Model
{
    protected $table = 'article_images';

    protected $fillable = [
    	'news_id',
    	'image',
    ];

    protected $appends = ['image_url'];

	public function getImageUrlAttribute()
    {
        return \Storage::disk('public')->url($this->image);
    } 

    public function product()
    {
    	return $this->belongsTo(Article::class);
    } 
}
