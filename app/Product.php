<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
	protected $fillable = [
		'name',
        'slug',
		'image',
		'author',
		'about_book',
		'isbn',
		'page_number',
		'cover_type',
		'decor',
		'illustration',
		'weight',
		'width',
		'length',
		'thickness',
        'preview',
        'price',
        'book_id',
        'video',
        'additional_info',
        'year',
        'pdf_url_service'
	];

	protected $casts = [
		'name' => 'string',
		'image' => 'string',
		'author' => 'string',
        'price' => 'string',
	];

	protected $appends = ['image_url', 'preview_url'];

	public static function rules(): array 
    {
        return [
            'name' => 'required|max:150',
            'image' => 'required',
            'author' => 'required|max:150',
            'price' => 'required',

        ];
    }	

	public function getImageUrlAttribute()
    {
        return \Storage::disk('public')->url($this->image);
    } 

    public function getPreviewUrlAttribute()
    {
        return \Storage::disk('public')->url($this->preview);
    }        

    public function characteristicTypes()
    {
        return $this->belongsToMany(CharacteristicType::class, 'product_part', 'product_id', 'characteristic_type_id');
    }

    public function characteristics()
    {
        return $this->belongsToMany(CharacteristicType::class, 'product_part', 'product_id', 'characteristic_id');
    }    

    public function filters()
    {
        return $this->belongsToMany(CharacteristicType::class, 'product_part', 'product_id', 'filter_id');
    } 

    public function comments()
    {
    	return $this->hasMany(Comment::class, 'product_id');
    }

    public function categories()
    {
    	return $this->belongsToMany(Category::class);
    }

    public function relatedProducts()
    {
        return $this->belongsToMany(self::class, 'product_series', 'same_product_id', 'product_id');
    }

    public function relatedSameProducts()
    {
        return $this->belongsToMany(self::class, 'product_series', 'product_id', 'same_product_id');
    }

    public function linksProductParts()
    {
        return $this->hasMany(LinksProductPart::class);
    }

    public function productImages()
    {
        return $this->hasMany(ProductImage::class);
    }

    public function promotion()
    {
        return $this->hasOne(Promotion::class);
    }

    public function audios()
    {
        return $this->hasMany(ProductAudio::class);
    }

    public function scopeFilterByName($query, $filter, $isbn)
    {
        if ($filter && $isbn) {
            return $query
                ->where('name', 'like', $filter)
                ->where('isbn', 'like', $isbn)
                ->orderBy('updated_at', 'DESC')
                ->paginate(8);
        } else if ($filter) {
            return $query
                ->where('name', 'like', $filter)
                ->orderBy('updated_at', 'DESC')
                ->paginate(8);
        } else if ($isbn) {
            return $query
                ->where('isbn', 'like', $isbn)
                ->orderBy('updated_at', 'DESC')
                ->paginate(8);
        } else {
            return $query
                ->orderBy('updated_at', 'DESC')
                ->paginate(8);
        }
    }

    public function scopeGetById($query, $id)
    {
    	return $query
    		->where('id', $id)
    		->first();
    }

    public function scopeAllBooks($query)
    {
        return $query
            ->select('id', 'name', 'author', 'image', 'price', 'slug')
            ->get();
    }

    public function scopeGetPromotions($query)
    {
        return $query
            ->select('id', 'name', 'author', 'image', 'price')
            ->where('is_promotion', true)
            ->get();
    }

    public function scopeGetNewBooks($query, $partId)
    {
        return $query
            ->select('products.id', 'products.name', 'products.author', 'products.image', 'products.price')
            ->where('is_new', true)
            ->join('product_part', function ($join) {
                $join->on('product_part.product_id', '=', 'products.id');
            })
            ->where('product_part.characteristic_type_id', $partId)
            ->groupBy('products.id')            
            ->orderBy('products.created_at')
            ->get();
    }

    public function scopeGetRecommendedBooks($query)
    {
        return $query
            ->select('id', 'name', 'author', 'image', 'price')
            ->where('is_recommended', true)
            ->get();
    }    


    public function scopeGetLinkedCharacteristics($query, $id)
    {
        return $query
            ->where('id', $id)
            ->characteristics;
    }   

    public function scopeFindProductsForArrayOfFilters($query, array $filters, int $characteristicTypeId)
    {
        $query = Product::select('products.id', 'products.name', 'products.price', 'products.image', 'products.author')
            ->leftJoin('product_filters_table', 'product_filters_table.product_id', '=', 'products.id')
            ->leftJoin('filters', 'product_filters_table.filter_id', '=', 'filters.id')
            ->groupBy('products.id');
        if (count($filters) !== 0) {
            foreach($filters as $key => $filter) {
                $query->where('filters.id', $filter);
            }
        } else {
            $query->leftJoin('characteristics', 'characteristics.id', '=', 'filters.characteristic_id')
                ->where('characteristics.characteristic_type_id', $characteristicTypeId);
        }

        return $query->paginate(8);
    }

    public function scopeGetFreeSameProducts($query, $productId)
    {
        return $query->where('products.id', '<>', $productId)
            ->leftJoin('product_series', function($join) use ($productId){
                $join->on('products.id', '=', 'product_series.same_product_id');
                $join->on('product_series.product_id', '=', \DB::raw("'".$productId."'"));
            })->whereNull('product_series.same_product_id')->select('products.id as id', 'products.name as name')->get();
    }   
}