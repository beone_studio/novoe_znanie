<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
	protected $fillable = [
		'name',
		'slug',
		'info',
		'video1',
		'video2',
		'image'
	];

	protected $casts = [
		'name' => 'string',
		'slug' => 'string',
		'video1' => 'string',
		'video2' => 'string',
		'image' => 'string'
	];

	public static function rules(): array 
    {
        return [
            'name' => 'required|max:150',
            'slug' => 'unique:articles|required|max:150',
            'video1' => 'nullable',
            'video2' => 'nullable',
            'image' => 'required'
        ];
    }

    protected $appends = ['image_url']; 

	public function getImageUrlAttribute()
    {
        return \Storage::disk('public')->url($this->image);
    } 

    public function images()
    {
        return $this->hasMany(NewsImage::class, 'news_id');
    }

    public function scopeGetById($query, $id)
    {
    	return $query
    		->where('id', $id)
    		->first();
    }

    public function scopeFilterByName($query, $filter)
    {
    	return $query
    		->where('name', 'like', $filter)
    		->paginate(8);
    }

    public function scopeGetWithPagination($query)
    {
        return $query
            ->orderBy('created_at', 'DESC')
            ->paginate(6);
    }    

    public function scopeGetBySlug($query, $slug)
    {
        return $query
            ->where('slug', $slug)
            ->first();
    }	

    public function scopeGetLastNews($query, $slug)
    {
        return $query
            ->latest()
            ->take(4)
            ->where('slug', '<>', $slug)
            ->get();
    }
}