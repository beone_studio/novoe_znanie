<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CharacteristicType extends Model
{
    protected $fillable = [
    	'name',
        'slug'
    ];

	protected $casts = [
		'name' => 'string',
        'slug' => 'string'
	];

	public static function rules(): array 
    {
        return [
            'name' => 'required|max:150',
            'slug' => 'required|max:150'
        ];
    }     

    public function characteristics() {
    	return $this->hasMany(Characteristic::class, 'characteristic_type_id');
    }

    public function books()
    {
        return $this->belongsToMany(Product::class, 'product_part', 'characteristic_type_id', 'product_id');
    }

    public function linksProductParts()
    {
        return $this->hasMany(LinksProductPart::class);
    }   

    public function banners()
    {
        return $this->hasMany(Banner::class);
    } 

    public function scopeGetBySlug($query, $slug)
    {
        return $query
            ->where('slug', $slug)
            ->first();
    }

    public function scopeGetById($query, $id)
    {
        return $query
            ->where('id', $id)
            ->first();
    }

    public function queryForChaining($query, $productId)
    {
        return $query
            ->leftJoin('product_part', function($join) use ($productId){
                $join->on('characteristic_types.id', '=', 'product_part.characteristic_type_id');
                $join->on('product_part.product_id', '=', \DB::raw("'" . $productId . "'"));
            })->select('characteristic_types.id as id', 'characteristic_types.name as name');
    }

    public function scopeGetChosenCharacteristicTypes($query, $productId)
    {
        return $this
            ->queryForChaining($query, $productId)
            ->where('product_part.product_id', $productId)
            ->groupBy('id')
            ->get();
    }

    public function scopeGetFreeCharacteristicTypes($query, $productId)
    {
        return $this
            ->queryForChaining($query, $productId)
            ->whereNull('product_part.characteristic_type_id')
            ->groupBy('id')
            ->get();
    }
}
