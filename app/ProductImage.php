<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductImage extends Model
{
    protected $fillable = [
    	'product_id',
    	'image',
    ];

    protected $appends = ['image_url'];

	public function getImageUrlAttribute()
    {
        return \Storage::disk('public')->url($this->image);
    } 

    public function product()
    {
    	return $this->belongsTo(Product::class);
    }    
}
