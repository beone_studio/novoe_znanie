<?php

namespace App;

use App\Product;
use App\Characteristic;
use Illuminate\Database\Eloquent\Model;

class Filter extends Model
{
    protected $fillable = [
        'name',
        'characteristic_id',
    ];

    protected $casts = [
        'name' => 'string',
        'characteristic_id' => 'integer',
    ];


    public static function rules(): array 
    {
        return [
            'name' => 'required|max:150',
            'characteristic_id' => 'required',
        ];
    }

    public function characteristic()
    {
    	return $this->hasOne(Characteristic::class);
    }

    public function books()
    {
        return $this->belongsToMany(Product::class, 'product_part', 'filter_id', 'product_id');
    }     

    public function scopeGetById($query, $id)
    {
    	return $query
    		->where('id', $id)
    		->first();
    }

    public function scopeGetFreeFiltersForCurrentProduct($query, $productId)
    {
        return $query
            ->leftJoin('product_filters_table', function($join) use ($productId){
                $join->on('filters.id', '=', 'product_filters_table.filter_id');
                $join->on('product_filters_table.product_id', '=', \DB::raw("'".$productId."'"));
            })->whereNull('product_filters_table.filter_id')->select('filters.id as id', 'filters.name as name')->get();
    }     
}
