<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Banner extends Model
{
    protected $fillable = [
    	'part_id',
    	'image',
        'url'
    ];

    protected $appends = ['image_url'];

	public function getImageUrlAttribute()
    {
        return \Storage::disk('public')->url($this->image);
    }    

    public function part()
    {
    	return $this->belongsTo(CharacteristicType::class);
    } 
}
